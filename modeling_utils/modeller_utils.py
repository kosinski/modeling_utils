#!/usr/bin/python
import sys
import os
import tempfile
from itertools import groupby, chain
if sys.version_info[0] == 2:
    from itertools import izip_longest, izip
    zip_longest = izip_longest
    zip = izip
    from cStringIO import StringIO
    range = xrange
elif sys.version_info[0] == 3:
    from itertools import zip_longest
    from io import StringIO

import string

from Bio import SeqIO

from modeller import environ, log, model, alignment, selection
from modeller.automodel import automodel, randomize
from modeller.scripts import complete_pdb

def fasta_iter(fh):
    """
    given a fasta file. yield tuples of header, sequence
    """
    # ditch the boolean (x[0]) and just keep the header or sequence since
    # we know they alternate.
    faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
    for header in faiter:
        # drop the ">"
        header = next(header)[1:].strip()
        # join all sequence lines to one.
        seq = "".join(s.strip() for s in next(faiter))
        yield header, seq

def partition(alist, indices):
    pairs = zip(chain([0], indices), chain(indices, [None]))
    return (alist[i:j] for i, j in pairs)

def _get_log_level():
    """Get current settings of modeller log level
    
    Modeller uses global log object for controlling log.
    Using _get_log_level enables to save old settings and later restore it.
    
    Usage:
    >>>from modeller import log
    >>>log.verbose() #log is now verbose
    >>>old = _get_log_level()
    >>>log.none()    #log is now none
    >>>log.level(*old) #log is verbose again
    
    Returns a tuple"""
    return log.output, log.notes, log.warnings, log.errors, log.memory

#def very_fast1(atmsel, actions):
#    """Very fast MD annealing"""
#    # at T=1000, max_atom_shift for 4fs is cca 0.15 A.
#    refine(atmsel, actions, cap=0.39, timestep=4.0,
#           equil_its=0, equil_equil=10,
#           equil_temps=(150.0, 400.0, 1000.0),
#           sampl_its=0, sampl_equil=100,
#           sampl_temps=(1000.0, 800.0, 500.0, 300.0))

from itertools import tee
def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)

class ModelWithBreaks(automodel):
    num_map = None
    chain_breaks = None

    def fix_numbering(self):
        if self.num_map:
            for old_resi, new_resi in zip(self.residues, self.num_map):
                # old_resi.num = str(new_resi+1) #this will work in new modeller release
                new_resi = new_resi + 1
                old_resi.num = "%4d " % new_resi


    def special_patches(self, aln):
        '''Take care so intra-chain breaks and real chain breaks are handled properly
        '''
        old_chain_names = [c.name for c in self.chains]
        if self.chain_breaks: #  Defines real chain breaks as based on predefined chain breaks.
            for c1, c2 in pairwise(self.chains):
                if int(c2.residues[0].index) not in self.chain_breaks:
                    c1.join(c2)

            #make nice chain names
            for i, c in enumerate(self.chains):
                c.name = old_chain_names[i]

        else:       #  Defines real chain breaks as columns where target and all templates had breaks.
            target_chain_index = -1
            target_seq = aln[-1]
            template_seqs = aln[:-1]
            had_chain_break = [False]*len(list(aln))
            target_chain_groups_indexes = [[]]
            for i, pos in enumerate(aln.positions):
                target_resi = pos.get_residue(target_seq)
                template_resis = [pos.get_residue(seq) for seq in template_seqs]

                if target_resi is not None:
                    if target_resi.index == target_resi.chain.residues[0].index or target_resi.index == target_resi.chain.residues[-1].index:
                        had_chain_break[0] = True
                        
                    else:
                        had_chain_break[0] = False

                for t_i, template_resi in enumerate(template_resis):                    
                    if template_resi is not None:
                        if template_resi.index == template_resi.chain.residues[0].index or template_resi.index == template_resi.chain.residues[-1].index:
                            had_chain_break[t_i+1] = True
                        else:
                            had_chain_break[t_i+1] = False

                if target_resi and target_resi.index == target_resi.chain.residues[0].index:
                    target_chain_index += 1

                if any(had_chain_break) and all(had_chain_break):
                    if target_chain_groups_indexes[-1]:
                        target_chain_groups_indexes.append([])
                else:
                    if target_resi:
                        target_chain = self.chains[list(target_seq.chains).index(target_resi.chain)]
                        if target_chain_index not in target_chain_groups_indexes[-1]:
                            target_chain_groups_indexes[-1].append(target_chain_index)


            subtract = 0
            for chain_group in target_chain_groups_indexes:
                if chain_group:
                    new_chain = self.chains[chain_group[0]-subtract]
                    for chain_index in chain_group[1:]:
                        new_chain.join(self.chains[chain_index-subtract])
                        subtract = subtract + 1

            # for c1, c2 in pairwise(self.chains):
            #     if int(c2.residues[0].index) not in self.chain_breaks:
            #         c1.join(c2)

            #make nice chain names
            for i, c in enumerate(self.chains):
                c.name = old_chain_names[i]

        self.rename_segments(segment_ids=[c.name for c in self.chains], renumber_residues=[1 for c in self.chains])

        for chain in self.chains:
            if not chain.residues[0].hetatm:
                self.patch(residue_type='NTER', residues=(chain.residues[0]))
            if not chain.residues[-1].hetatm:
                self.patch(residue_type='CTER', residues=(chain.residues[-1]))

        # self.fix_numbering()

    def user_after_single_model(self):
        self.fix_numbering()

class NoSuchResidue(Exception):
    pass

class SmartAutomodel(ModelWithBreaks):
    def get_num_maps_by_chain(self):
        breaks = []
        for c in self.chains:
            if breaks:
                breaks.append(breaks[-1]+len(c.residues))
            else:
                breaks.append(len(c.residues))
        del breaks[-1]

        return [list(g) for g in partition(self.num_map, breaks)]

    def get_mod_resi_index(self, resi, chainNo=0):
        '''Return model residue index (starts from 1)'''
        num_maps = self.get_num_maps_by_chain()

        try:
            num_map = num_maps[chainNo]
            offset = sum([len(nmap) for nmap in num_maps[:chainNo]])
        except IndexError:
            raise IndexError("There is no chain with number {0}. Note: chainNo arg numbers chains starting from 0".format(chainNo))

        try:
            resi_index = num_map.index(resi-1)
        except ValueError:
            raise NoSuchResidue("No residue with number "+str(resi))

        index = offset + resi_index + 1 #model indexes start from 1

        return index

    def get_mod_resi(self, resi, chainNo=0):
        '''Return mod residue num from origina residue num'''
        index = self.get_mod_resi_index(resi, chainNo)

        return self.residues[index-1].num

    def get_mod_resi_range(self, resi1, resi2, chainNo=0):
        # for r in self.residues:
        #     print(r.index, r.num)
        index1 = self.get_mod_resi_index(resi1, chainNo=chainNo)
        index2 = self.get_mod_resi_index(resi2, chainNo=chainNo)

        rng = self.residue_range(index1-1, index2-1) #model resi indices are from 1, but for residue_range is from 0

        return self.residue_range(index1, index2)

def run_modeller(alnfile,
                 knowns,
                 sequence,
                 cwd=None,
                 atom_files_dirs=None,
                 very_fast=False,
                 starting_models=1,
                 ending_models=1,
                 with_breaks=False,
                 mdl_class=None,
                 ligands=False,
                 num_map=None,
                 md_level=None,
                 max_var_iterations=None,
                 rand_method=randomize.xyz,
                 max_ca_ca_distance=14,
                 chain_breaks=None):
    """
    with_breaks - all '/' in alnfile will be model as chain breaks, the model will be single chain. 
    """
    # Very fast homology modeling by the automodel class
    old = _get_log_level()
    old_cwd = os.getcwd()
    

    try:
        log.none() #this does not prevent logs from a.make() :-(
        if atom_files_dirs is None:
            atom_files_dirs=['.']
            
        
        if cwd is not None:
            os.chdir(cwd)
            
        env = environ()

        ModelClass = mdl_class or automodel
        if with_breaks and mdl_class is None:
            env.patch_default = False        
            ModelClass = ModelWithBreaks
            
        if mdl_class:
            env.patch_default = False      
        # directories for input atom files
        env.io.atom_files_directory = atom_files_dirs

        if ligands:
            env.io.hetatm = True

        knowns = list(map(str, knowns))
        print(knowns)
        a = ModelClass(env,
                  alnfile=alnfile,      # alignment filename
                  knowns=knowns,                # codes of the templates
                  sequence=sequence)              # code of the target
        
        if hasattr(a, 'num_map'):
            a.num_map = num_map

        if hasattr(a, 'chain_breaks'):
            a.chain_breaks = chain_breaks
            

        a.rand_method = rand_method
        a.max_ca_ca_distance = max_ca_ca_distance
        if very_fast:
            a.very_fast()                       # prepare for extremely fast optimization
            a.md_level = None
            a.max_var_iterations = 3

        if md_level is not None:
            a.md_level = md_level

        if max_var_iterations is not None:
            a.max_var_iterations = max_var_iterations

        a.starting_model = starting_models
        a.ending_model = ending_models


        a.make()                        # make the homology model
        

        out_model_filenames = []
        for out_model in a.outputs:
            out_model_filenames.append(os.path.join(cwd, out_model['name']))
            
        return out_model_filenames
    
    finally:
        log.level(*old)
        os.chdir(old_cwd)


def run_modeller_from_fasta(alnfilename, knowns, sequence, cwd=None, atom_files_dirs=None, skip_tails=False, skip_inserts=False, skipNter=False, skipCter=False, flanks=0, with_breaks=False, **kwargs):
    """
    Arguments:
    o knowns - tuple - tuple of pdb codes
    o sequence - string - target sequence name"""
    with open(alnfilename) as alnfile:
        piraln, num_map = create_modeller_pir_from_fasta(alnfile, skip_tails=skip_tails, skip_inserts=skip_inserts)

    pirfilehandle, pirfilename = tempfile.mkstemp()
    os.close(pirfilehandle)
    with open(pirfilename, 'w') as pirfile:
        pirfile.write(piraln)

    out_model_filenames = run_modeller(pirfilename, knowns, sequence, cwd, atom_files_dirs, num_map=num_map, with_breaks=with_breaks, **kwargs)
    
    os.remove(pirfilename)
    return out_model_filenames


def run_modeller_from_aln_data(aln_data, knowns, sequence, cwd=None, atom_files_dirs=None, skip_tails=False, skip_inserts=False, skipNter=False, skipCter=False, flanks=0, **kwargs):
    """
    Arguments:
    o knowns - tuple - tuple of pdb codes
    o sequence - string - target sequence name"""

    piraln, num_map, alnobj = create_modeller_pir(aln_data, skip_tails=skip_tails, skip_inserts=skip_inserts)

    pirfilehandle, pirfilename = tempfile.mkstemp()
    os.close(pirfilehandle)
    with open(pirfilename, 'w') as pirfile:
        pirfile.write(piraln)
    
    out_model_filenames = run_modeller(pirfilename, knowns, sequence, cwd, atom_files_dirs, num_map=num_map, **kwargs)
    
    os.remove(pirfilename)

    return out_model_filenames




class ModelPepcomposer(automodel):
    """
    renames chains instead of joining them because joining first two chains never work in multimers! (BUG?)
    """
    def special_patches(self, aln):
        

        self.rename_chains()


#        for chain in self.chains:
#            self.patch(residue_type='NTER', residues=chain.residues[0])
#            self.patch(residue_type='CTER', residues=chain.residues[-1])
            

        
        self.transfer_numbering()
#        for attr in dir(self.env):
#          print("obj.%s = %s" % (attr, getattr(self.env, attr)))
          
        
    def rename_chains(self):
        with open(self.alnfile) as f:
            lines = f.readlines()
        seq_data = [lines[2], lines[5], lines[8]]
        aln_cols = list(map(list, zip(*seq_data)))

        join_spec = []
        join_chains = []
        chain_id = 0
        joints = []
        
        ref_chain_names = list(reversed([c.name for c in self.chains]))
        ref_chain_names = list(reversed(self.get_out_chain_list()))
        ref_chain_names.pop()

        for col in aln_cols:
            target = col[0]
            t1 = col[1]
            t2 = col[2]
            if target == '/':
                if t1 == '/' and t2 == '/':
                    self.chains[chain_id+1].name = ref_chain_names.pop()
                else:
                    self.chains[chain_id+1].name = self.chains[chain_id].name
                chain_id = chain_id + 1
                
    def transfer_numbering(self):
        env = environ()
        env.io.atom_files_directory = self.env.io.atom_files_directory
        template_code = self.knowns[0]

        template = model(env)
        template.read(template_code)
        
        i = 1
        for mdl_resi, templ_resi in zip_longest(self.residues, template.residues, fillvalue='X'): 
            if templ_resi != 'X':
                mdl_resi.num = templ_resi.num
            else:
                mdl_resi.num = '{0}'.format(i)
                i = i + 1

    def get_out_chain_list(self):
        env = environ()
        env.io.atom_files_directory = self.env.io.atom_files_directory
        t1_code = self.knowns[0]
        t2_code = self.knowns[1]

        t1 = model(env)
        t1.read(t1_code)
        
        t2 = model(env)
        t2.read(t2_code)
        
        chains = []
        for c in t1.chains:
            chains.append(c.name)
        
        #get only the last chain name from 2 (peptide)
        chains.append(t2.chains[len(t2.chains)-1].name)
        
        
        return chains
        
def run_modeller_pepcomposer(aln_data, cwd=None, atom_files_dirs=None, **kwargs):
    """
    note: due to bug in Chain.join() in Modeller before 9v11 it uses lots of workarounds to properly join chains.
    """
    sequence = aln_data[0][0]
    knowns = [s[0] for s in aln_data[1:]]
    model_filenames = run_modeller_from_aln_data(aln_data, knowns, sequence, cwd, atom_files_dirs, mdl_class=ModelPepcomposer, **kwargs)

    old = _get_log_level()
    old_cwd = os.getcwd()
    
    outfilename = None
    try:
        log.none() #this does not prevent logs from a.make() :-(
        if cwd is not None:
            os.chdir(cwd)
                
        env = environ()
        env.libs.topology.read('${LIB}/top_heav.lib')
        env.libs.parameters.read('${LIB}/par.lib')
        mdl = model(env)
        mdl.read(file=model_filenames[0])
    
        tempfilehandle, no_ter_filename = tempfile.mkstemp()
        os.close(tempfilehandle) #I think it is necessary to close the handle to tempfile, otherwise I can get too many open files error
        
        mdl.write(file=no_ter_filename, no_ter=True)
        mdl = complete_pdb(env, no_ter_filename, transfer_res_num=True)
            
        outfilename = os.path.join(cwd, sequence + '.pdb')
        
        mdl.write(file=outfilename)

    finally:
        log.level(*old)
        os.chdir(old_cwd)
    
    return outfilename




#class WrongPdbCodeError(Exception):
#    def __init__(self, pdbcode):
#        print('WARNING! Template name is "{0}", template names must be like "pdbcodeChain" e.g. "1xxxA"'.format(pdbcode))
#
#def validate_pdbcode(pdbcode):
#    if len(pdbcode) != 5:
#        raise WrongPdbCodeError(pdbcode)


class Aln(object):
    ori_aln = None
    templ_seq_to_atom_num = []
    def __init__(self, aln):
        self.aln = aln

    def remove_tails_inserts(self, skip_tails=False, skip_inserts=False, skipNter=False, skipCter=False, skip_missing=False, flanks=0):
        """
        o skip_missing - when true insertions to missing (lowercase letters) are treated as breaks '/'
        o flanks - how many aa at flanks of tails and insertions to keep
        All gap columns get compressed

        Can this be substituted with modifying model class with?:
        # model.get_insertions(), model.get_deletions() and model.loops() now
          have an include_termini option; if False, residue ranges that include
          chain termini are excluded from the output.

        """
        def print_seqs(seqs):
            for seq in seqs:
                print(seq)
                
        def is_insert_col(col):
            col_chars = set(col[1:])
            if len(col_chars) == 1 and col_chars.pop() == '-' and col[0] not in ('-',):  
                return True
            else:
                return False
            
        def is_range_insert(col_range):
            first_col = col_range[0]
            if is_insert_col(first_col):
                return True
            else:
                return False
            


        def is_target_nongap_resi(col):
            if col[0] not in ('-', '/'):
                return True
            else:
                return False

        def is_missing(col):
            lower_found = False
            for c in col[1:]:
                if c.isupper():
                    return False
                if c.islower():
                    lower_found = True
            if lower_found:
                return True
            else:
                return False #was insert col or aligned with not a letter

        def is_range_missing(col_range):
            first_col = col_range[0]
            if is_missing(first_col):
                return True
            else:
                return False



        def grouper(col):
            if is_insert_col(col):
                return 0
            elif is_missing(col):
                return 1
            else:
                return 2

        def missing_to_gaps(col_range):
            for col in col_range:
                for i in range(len(col)):
                    c = col[i]
                    if c.islower():
                        col[i] = '-'
        if not self.ori_aln:
            self.ori_aln = self.aln[:]

        ori_new_mapping = [] #a list with length of target seq with no inserts/tails, index is index of resi in that new seq, every item holds original target numbering (from 0)
        out_aln = []
        out_aln_cols = []

        if skip_tails or skip_inserts or skip_missing or skipNter or skipCter:
            seqs = [seq_data[1] for seq_data in self.aln]
            no_of_seqs = len(seqs)

            aln_cols = list(map(list, zip(*seqs)))

            # split_by_chain = groupby(aln_cols, lambda x: x[0] == '/' and x[1] == '/')
            split_by_chain = [list(group) for k, group in groupby(aln_cols, lambda x: len(set(x)) == 1 and set(x).pop() == '/') if not k]


            for chain_count, chain_aln_cols in enumerate(split_by_chain):
                col_ranges = [list(g) for k, g in groupby(chain_aln_cols, grouper)]

                out_ranges = []

                target_num = 0
                chain_seq = ''.join([col[0] for col in chain_aln_cols]).replace('-', '').replace('/','')

                # target_len = len(seqs[0].replace('-', '').replace('/',''))
                target_len = len(chain_seq)

                for col_range in col_ranges:
                    is_tail = False
                    is_n_tail = False
                    is_c_tail = False
                    if is_range_insert(col_range):
                        if target_num == 0:
                            is_tail = True
                            is_n_tail = True        


                    target_nums = []
                    for col in col_range:
                        if is_target_nongap_resi(col):
                            if skip_missing:
                                if not is_missing(col):
                                    target_nums.append(target_num)
                            else:
                               target_nums.append(target_num)
                            
                            target_num = target_num + 1
                            
                    
                    if is_range_insert(col_range):
                        if target_num == target_len:
                            is_tail = True
                            is_n_tail = False
                            is_c_tail = True


                    if skip_tails and is_tail:
                        #TODO: what if there are missing ? just remove continue?
                        if flanks > 0:
                            if is_n_tail:
                                col_range = col_range[-flanks:]
                                target_nums = target_nums[-flanks:]
                            elif is_c_tail:
                                col_range = col_range[:flanks]
                                target_nums = target_nums[:flanks]
                            out_ranges.append(col_range)
                            ori_new_mapping.extend(target_nums)
                        continue


                    elif skipNter and is_n_tail:
                        continue

                    elif skipCter and is_c_tail:
                        continue

                    elif is_range_insert(col_range):
                        if skip_inserts and not is_tail:
                            break_col = [['$'] + ['-' for i in range(no_of_seqs-1)]] # '$'' will be replaced to '/' in get_pir
                            if flanks > 0:
                                if 2*flanks < len(col_range):
                                    col_range = col_range[:flanks] + break_col + col_range[flanks:][-flanks:]
                                target_nums = target_nums[:flanks] + target_nums[flanks:][-flanks:]
                                out_ranges.append(col_range)
                                ori_new_mapping.extend(target_nums)
                            else:
                                out_ranges.append(break_col)
                            continue



                    if skip_missing and is_range_missing(col_range):
                        col_range = [['-' for c in col_range[0]]]
                        col_range[0][0] = '$' ## '$'' will be replaced to '/' in get_pir (i.e. to break character for modeller)
                    
                    if skip_missing:
                        missing_to_gaps(col_range)

                    
                    out_ranges.append(col_range)
                    ori_new_mapping.extend(target_nums)

                col_ranges = out_ranges


                for col_range in col_ranges:
                    out_aln_cols.extend(col_range)

                if len(split_by_chain) > 1 and chain_count < len(split_by_chain)-1:
                    chain_sep_col = ['/' for i in range(no_of_seqs)]
                    out_aln_cols.append(chain_sep_col)
            #Rebuild aln data
            out_seqs = [''.join(seq) for seq in zip(*out_aln_cols)]

            
            i = 0
            for seq_data in self.aln:
                old_seq_name = self.aln[i][0]
                new_seq = out_seqs[i]
                out_aln.append([old_seq_name, new_seq])
                i = i + 1    
        
        else:
            target_seqs = self.aln[0][1].replace('-', '').split('/')
            ori_new_mapping = []
            i = 0
            for target_seq in target_seqs:
                target_seq_len = len(target_seq)
                ori_new_mapping.extend([i for i in range(target_seq_len)])
            out_aln = self.aln


        self.aln = out_aln
        self.ori_new_mapping = ori_new_mapping

    def adjust_include_regions(self, include_regions):
        seqs = [seq_data[1] for seq_data in self.aln]


        #adjust regions for the target
        out_target_seqs = []
        target_regions = include_regions[0]
        target_seqs = seqs[0].split('/')

        ori_new_mapping_out = []
        resi_i = 0
        was_removing = False
        target_include_regions = include_regions[0][1:]
        for seq_i, target_seq in enumerate(target_seqs):
            out_target_seq = []
            for resi in target_seq:
                if self.is_nongap_resi(resi):
                    if not self._is_in_ranges(self.ori_new_mapping[resi_i]+1, target_include_regions[seq_i]):
                        out_target_seq.append('-')
                        was_removing = True
                    else:
                        if was_removing:
                            out_target_seq[-1] = '$'
                            was_removing = False
                        ori_new_mapping_out.append(resi_i)
                        out_target_seq.append(resi)
                    resi_i  = resi_i + 1
                else:
                    out_target_seq.append(resi)

            out_target_seqs.append(''.join(out_target_seq))


        self.aln[0][1] = '/'.join(out_target_seqs)
        self.ori_new_mapping = ori_new_mapping_out

        #adjust regions for the templates
        self.gen_templ_seq_to_atom_num()

        seqs = [seq_data[1] for seq_data in self.aln]
        no_of_seqs = len(seqs)
        aln_cols = list(map(list, zip(*seqs)))

        for seq_i in range(1, len(seqs)):
            templ_include_regions = include_regions[seq_i][1:]
            resi_i = 0
            chain_idx = 0
            was_removing = False
            segment_to_add = []
            out_aln_cols = []
            seqlen = len(seqs[seq_i].replace('-','').replace('$','').replace('/',''))
            for col in aln_cols:

                resi = col[seq_i]
                if resi == '/':
                    chain_idx = chain_idx + 1
                if self.is_nongap_resi(resi):
                    templ_seq_to_atom_num = self.templ_seq_to_atom_num[seq_i-1]
                    seq_resi_i = templ_seq_to_atom_num[resi_i]
                    if not self._is_in_ranges(seq_resi_i, templ_include_regions[chain_idx]):
                        new_col = ['-' for i in range(len(col))]
                        new_col[seq_i] = col[seq_i]
                        segment_to_add.append(new_col)
                        col[seq_i] = '-'
                        was_removing = True
                        out_aln_cols.append(col)
                        if resi_i == (seqlen-1) and was_removing:
                            tempseqs = [''.join(seq) for seq in zip(*out_aln_cols)]
                            for new_col in segment_to_add:
                                out_aln_cols.append(new_col)
                            was_removing = False
                            segment_to_add = []

                    else:
                        if was_removing:
                            for new_col in segment_to_add:
                                out_aln_cols.append(new_col)
                            was_removing = False
                            segment_to_add = []
                        out_aln_cols.append(col)
                    resi_i  = resi_i + 1
                else:
                    out_aln_cols.append(col)

            aln_cols = out_aln_cols

        #Rebuild aln data
        out_seqs = [''.join(seq) for seq in zip(*out_aln_cols)]

        out_aln = []
        i = 0
        for seq_data in self.aln:
            old_seq_name = self.aln[i][0]
            new_seq = out_seqs[i]
            out_aln.append([old_seq_name, new_seq])
            i = i + 1    

        self.aln = out_aln

    def gen_templ_seq_to_atom_num(self):
        self.templ_seq_to_atom_num = []
        for name, seq in self.aln[1:]:
            self.templ_seq_to_atom_num.append(list(map(int, extract_modeller_atom_seq_with_numbers(name)[1])))

    def is_nongap_resi(self, resi):
        if resi not in ('-', '/', '$'): # $ is for intra-chain breaks
            return True
        else:
            return False

    def _is_in_ranges(self, resi_id, ranges):
        for first_resi, last_resi in ranges:
            if resi_id in range(first_resi, last_resi+1):
                return True

        return False

    def is_multichain(self):
        seqs = [seq_data[1] for seq_data in self.aln]
        for seq in seqs:
            if '/' in seq:
                return True

        return False

    def get_pir(self, chain_config=None, pdbfilenames=None):
        if chain_config is None:
            chain_config = {}
        target_name = self.aln[0][0]
        pir = []
        for seq_item in self.aln:
            name = seq_item[0]

            seq = seq_item[1]
            seq = seq.replace('$', '/') # $ is for intra-chain breaks
            #Change seq string to multiline 75 chars each
            #seq = "\n".join([seq[i:i+75] for i in range(0, len(seq), 75)])


            out_seq = seq + "*"

            if name == target_name:
                stype = 'sequence'            
                out_name = '>P1;' + name
                comment = '{0}:{1}: : : : : : : :'.format(stype, name)

            else:
                chain = chain_config.get(name)

                if chain is None:
                    chain = '.' #if no chain, Modeller requires '.'
                
                stype = 'structure'
                
                if chain != '.':
                    out_name = '>P1;' + name
                else:
                    out_name = '>P1;' + name
                
                pdb_filename_or_code = name
                if pdbfilenames is not None and name in pdbfilenames:
                    pdb_filename_or_code = pdbfilenames[name]

                comment = '{0}:{1}: :{2}: :{2}: : : :'.format(stype, pdb_filename_or_code, chain)


            pir.append(out_name)
            pir.append(comment)
            pir.append(out_seq)

        return '\n'.join(pir)


    def get_fasta(self):
        for seq_item in self.aln:
            name = seq_item[0]
            seq = seq_item[1] 
            print('>'+name)
            print(seq)



def create_modeller_pir(aln, chain_config=None, skip_tails=False, skip_inserts=False, skipNter=False, skipCter=False, flanks=0, ligands=False, ligands_cfg=None, pdbfilenames=None, include_regions=None):
    """
    Arugments:
    o aln -- list of lists -- [['target_name', 'seq1'], ['template_name1', 'seq2']], the first is the target
    o chain_config {'template_name1': 'A'} -- specifies chain names for templates, 
                if not specified - '.' is used in pir alignment.
    o ligands_cfg e.g.

    
    [
    ['C', []], #include whole chain C
    ['E', [400, 401]] #include resi 400 and 401 from chain E
    ]
    """
    
    def _is_in_ligands_cfg(chain, resi_num, ligands_cfg):
        for spec in ligands_cfg:
            if spec[0] == chain:
                if len(spec[1]) == 0:
                    return True

                if resi_num in spec[1]:
                    return True
        return False
    

    aln = Aln(aln)
    



    aln.remove_tails_inserts(skip_tails=skip_tails, skip_inserts=skip_inserts, skipNter=skipNter, skipCter=skipCter, flanks=flanks)
    if include_regions:
        aln.adjust_include_regions(include_regions)


    if ligands and pdbfilenames:
        old = _get_log_level()
        

        try:

            full_alns = []
            for seq_name, pdbfilename in pdbfilenames.items():
                fh = open(pdbfilename) #to raise error if file doesn't exist
                fh.close()


                tempfilehandle, tempfilename = tempfile.mkstemp()
                os.close(tempfilehandle) #I think it is necessary to close the handle to tempfile, otherwise I can get too many open files error


                root, ext = os.path.splitext(pdbfilename)
    
                log.none()
                env = environ()
                env.io.hetatm = True
                mdl = model(env, file=pdbfilename)



                

                aln_cols = []
                for r in mdl.residues:
                    if _is_in_ligands_cfg(r.chain.name, int(r.num), ligands_cfg):
                        col = ['.', '.']
                    else:
                        col = ['-', '.']
                
                    aln_cols.append([r.chain.name, col])

               
                aln_cfg = []
                
                for chain, g in groupby(aln_cols, lambda x: x[0]):
                    c_aln_cols = []
                    for col in g:
                        c_aln_cols.append(col[1])
                    out_seqs = [''.join(seq) for seq in zip(*c_aln_cols)]

                    aln_cfg.append([chain, out_seqs])



                full_alns.append(aln_cfg)

                #remove:
                test_str = ''
                for a in aln_cfg:
                    test_str += a[1][1] + '/'

                alntemp = alignment(env)

                root, ext = os.path.splitext(pdbfilename)
                alntemp.append_model(mdl, align_codes=os.path.basename(root))
                alntemp.write(file=tempfilename)

                with open(tempfilename) as f:
                    seq = SeqIO.read(f, 'pir')


        finally:
            log.level(*old)     


    pir_str = aln.get_pir(chain_config=chain_config, pdbfilenames=pdbfilenames)
    
    return pir_str, aln.ori_new_mapping, aln

def create_modeller_pir_structures(aln, chain_config=None, skip_tails=False, skip_inserts=False, skipNter=False, skipCter=False, pdb_filenames=None):
    """
    PIR for aln-dependent superpositions
    Arugments:
    o aln -- list of lists -- [['struct_name1', 'seq1'], ['struct_name2', 'seq2']], the first is the target
    o chain_config {'template_name1': 'A'} -- specifies chain names for templates, 
                if not specified - '.' is used in pir alignment.
    o pdb_filenames - mapping {seq_name: pdbfilename} in case seq name is different from pdb name
    """
    if chain_config is None:
        chain_config = {}
    target_name = aln[0][0]
    aln = Aln(aln)
    aln.remove_tails_inserts(skip_tails=skip_tails, skip_inserts=skip_inserts, skipNter=skipNter, skipCter=skipCter)

    pir = []
    for seq_item in aln.aln:
        name = seq_item[0]

        seq = seq_item[1]
        #Change seq string to multiline 75 chars each
        #seq = "\n".join([seq[i:i+75] for i in range(0, len(seq), 75)])

        seq = seq.replace('$', '/')
        out_seq = seq + "*"


        chain = chain_config.get(name)

        if chain is None:
            chain = '.' #if no chain, Modeller requires '.'
        
        type = 'structure'
        
        pdb_filename = None
        if pdb_filenames:
            pdb_filename = pdb_filenames.get(name)
            
        
        if chain != '.':
            out_name = '>P1;' + name
        else:
            out_name = '>P1;' + name
        
        if pdb_filename is None:
            pdb_filename = name

        comment = '{0}:{1}: :{2}: :{2}: : : :'.format(type, pdb_filename, chain)


        pir.append(out_name)
        pir.append(comment)
        pir.append(out_seq)

    pir_str = '\n'.join(pir)
    
    return pir_str


def aln_file_to_aln_data(f):
    aln = []
    for name, seq in fasta_iter(f):
        aln.append([name, seq])

    return aln

# def create_modeller_pir_from_fasta(file_handle, skip_tails=False, skip_inserts=False):
def create_modeller_pir_from_fasta(file_handle, *args, **kwargs):
    """
    Arugments:
    o target_name - name of the target sequences, if not specified, first seq is target
    """
    aln_data = aln_file_to_aln_data(file_handle)
    #pir_str, num_map, alnobj = create_modeller_pir(aln, skip_tails=skip_tails, skip_inserts=skip_inserts)
  
    piraln, num_map, alnobj = create_modeller_pir(aln_data, *args, **kwargs)

    return piraln, num_map

    
def extract_modeller_atom_seq(pdbfilename):
    old = _get_log_level()
    tempfilehandle, tempfilename = tempfile.mkstemp()
    os.close(tempfilehandle) #I think it is necessary to close the handle to tempfile, otherwise I can get too many open files error
    try:
        root, ext = os.path.splitext(pdbfilename)
        log.none()
        env = environ()
        
    #    log.level(output=1, notes=1, warnings=1, errors=1, memory=0)
        mdl = model(env, file=pdbfilename)
        aln = alignment(env)

        aln.append_model(mdl, align_codes=os.path.basename(root))
    
        
    
        aln.write(file=tempfilename)
        with open(tempfilename) as f:
            seq = SeqIO.read(f, 'pir')
    
        return str(seq.seq)
    finally:
        log.level(*old)
        os.remove(tempfilename)


#def catch_modeller_logs(logdir, stdout_logfile, stderr_logfile, fn):
#def catch_modeller_logs(logdir) :
#    print(logdir)

def extract_modeller_atom_seq_with_numbers(pdbfilename):
    old = _get_log_level()
    tempfilehandle, tempfilename = tempfile.mkstemp()
    os.close(tempfilehandle) #I think it is necessary to close the handle to tempfile, otherwise I can get too many open files error
    try:
        root, ext = os.path.splitext(pdbfilename)
        log.none()
        env = environ()
        
    #    log.level(output=1, notes=1, warnings=1, errors=1, memory=0)
        mdl = model(env, file=pdbfilename)
        aln = alignment(env)
        aln.append_model(mdl, align_codes=os.path.basename(root))
    
        aln.write(file=tempfilename)
        with open(tempfilename) as f:
            seq = SeqIO.read(f, 'pir')
    
        return [str(seq.seq), [resi.num for resi in mdl.residues]]
    finally:
        log.level(*old)
        os.remove(tempfilename)

def get_B_factors_resi(pdbfilename, chain=None):
    if chain is None:
        chain = 0
    old = _get_log_level()
    tempfilehandle, tempfilename = tempfile.mkstemp()
    os.close(tempfilehandle) #I think it is necessary to close the handle to tempfile, otherwise I can get too many open files error
    resi_B_factors = []
    try:
        root, ext = os.path.splitext(pdbfilename)
        log.none()
        env = environ()
        
    #    log.level(output=1, notes=1, warnings=1, errors=1, memory=0)
        mdl = model(env, file=pdbfilename)
#        aln = alignment(env)
#        aln.append_model(mdl, align_codes=root)
#    
#        aln.write(file=tempfilename)
#        with open(tempfilename) as f:
#            seq = SeqIO.read(f, 'pir')
        for resi in mdl.chains[chain].residues:
            valsB = []
            for atom in resi.atoms:
                valsB.append(atom.biso)
                
            len_valsB = len(valsB)
            mean = sum(valsB)/len_valsB if len_valsB > 0 else float('nan')

            resi_B_factors.append({
                                   'resi': resi.num,
                                   'b': mean
                                   }
                                  )
        return resi_B_factors
    finally:
        log.level(*old)
        os.remove(tempfilename)

def translate_rotate_pdb(pdbfilename, out_pdbfilename, translate=None, rotate=None, angle_axis=None):
    """
    First translate then rotate
    o translate - [1, 0, 0]
    o rotate - [[1, 0, 0],
                         [0, 1, 0],
                         [0, 0, 1]]
    """
    #Convert to normal str, modeller crashes on unicode
    pdbfilename = str(pdbfilename)
    out_pdbfilename = str(out_pdbfilename)
    
    old = _get_log_level()
    tempfilehandle, tempfilename = tempfile.mkstemp()
    os.close(tempfilehandle) #I think it is necessary to close the handle to tempfile, otherwise I can get too many open files error
    try:
        log.none()
        env = environ()
        env.io.hetatm = True
#        env.io.water = True
        mdl = model(env, file=pdbfilename)
        

        # Select all atoms
        s = selection(mdl)

        # Translate 1 angstrom along the x axis:
        if translate:
            s.translate(translate)    
                
#        # Transform with a rotation matrix:
        if rotate:
            s.transform(rotate)
        elif angle_axis:
            angle = angle_axis[0]
            axis = angle_axis[1]
            s.rotate_origin(axis, angle)

        mdl.write(file=out_pdbfilename)
    
        return True
    finally:
        log.level(*old)
        os.remove(tempfilename)    

def rotate_translate_pdb(pdbfilename, out_pdbfilename, translate=None, rotate=None, angle_axis=None):
    """
    First rotate then translate
    o translate - [1, 0, 0]
    o rotate - [[1, 0, 0],
                         [0, 1, 0],
                         [0, 0, 1]]
    """
    #Convert to normal str, modeller crashes on unicode
    pdbfilename = str(pdbfilename)
    out_pdbfilename = str(out_pdbfilename)
    
    old = _get_log_level()
    tempfilehandle, tempfilename = tempfile.mkstemp()
    os.close(tempfilehandle) #I think it is necessary to close the handle to tempfile, otherwise I can get too many open files error
    try:
        log.none()
        env = environ()
        env.io.hetatm = True
#        env.io.water = True
        mdl = model(env, file=pdbfilename)
        

        # Select all atoms
        s = selection(mdl)

#        # Transform with a rotation matrix:
        if rotate:
            s.transform(rotate)
        elif angle_axis:
            angle = angle_axis[0]
            axis = angle_axis[1]
            s.rotate_origin(axis, angle)

        # Translate 1 angstrom along the x axis:
        if translate:
            s.translate(translate)    
                


        mdl.write(file=out_pdbfilename)
    
        return True
    finally:
        log.level(*old)
        os.remove(tempfilename) 

def refine_aln_fit(alnfile,
                 knowns,
                 cwd=None,
                 atom_files_dirs=None
                 ):


    old = _get_log_level()
    old_cwd = os.getcwd()
    

    try:
        log.none() #this does not prevent logs from a.make() :-(
        if atom_files_dirs is None:
            atom_files_dirs=['.']
        
        
        if cwd is not None:
            os.chdir(cwd)
            
        env = environ()
        env.io.atom_files_directory = atom_files_dirs
        
        aln = alignment(env)
        aln.append(file=alnfile, align_codes=knowns)
        aln.align3d(gap_penalties_3d=[0, 4.0], align3d_repeat=True, output='SHORT')
#        aln.salign(gap_penalties_3d=[0, 4.0],
#                   feature_weights=(0., 1., 0., 0., 0., 0.),
#                   fit=False
#                   )
        aln.write(file=os.path.join(cwd, 'out.ali'))
        
        mdl = model(env, file=knowns[0])
        atmsel = selection(mdl).only_atom_types('CA')
        mdl2 = model(env, file=knowns[1])
        atmsel.superpose(mdl2, aln)
        mdl2.write(file=os.path.join(cwd, 'out.pdb'))


    
    finally:
        log.level(*old)
        os.chdir(old_cwd)

def extend_aln_fit(alnfile,
                 knowns,
                 cwd=None,
                 atom_files_dirs=None
                 ):


    old = _get_log_level()
    old_cwd = os.getcwd()
    

    try:
        log.very_verbose() #this does not prevent logs from a.make() :-(
        if atom_files_dirs is None:
            atom_files_dirs=['.']
        
        
        if cwd is not None:
            os.chdir(cwd)
            
        env = environ()
        env.io.atom_files_directory = atom_files_dirs
        
        aln = alignment(env, file=alnfile, align_codes=knowns)

        aln.salign(
                   fix_offsets=(0, 10, 20, 30, 40),
#                   gap_penalties_2d=(0, 0, 0, 0, 0, 0, 0, 0, 0),
                   feature_weights=(1., 1.0, 1., 1., 1., 0.),
#                   local_alignment=False,
#                   gap_penalties_1d=(-600, -400)
                   fit=True,
                   write_fit=True,
                   write_whole_pdb=True,
                   output='ALIGNMENT QUALITY'           
           
           
           ) 
        aln.write(file=os.path.join(cwd, 'out.ali'))
    
    finally:
        log.level(*old)
        os.chdir(old_cwd)

def fit_from_seq_aln(alnfile,
                 cwd=None,
                 atom_files_dirs=None
                 ):


    old = _get_log_level()
    old_cwd = os.getcwd()
    
    out = {}
    try:
        log.none() #this does not prevent logs from a.make() :-(
        if atom_files_dirs is None:
            atom_files_dirs=['.']
        
        
        if cwd is not None:
            os.chdir(cwd)
            
        env = environ()
        env.io.atom_files_directory = atom_files_dirs
        
        aln = alignment(env, file=alnfile, align_codes='all')
#        print('aln[1].atom_file before', aln[1].atom_file)
        ori_pdbfile_names = [s.atom_file for s in aln]

        aln.salign(
                   feature_weights=(1., 1., 1., 1., 1., 0.),
                   fit=True,
                   write_fit=True,
                   write_whole_pdb=True,
                   output='ALIGNMENT QUALITY'           
           
           
           )
    


#        aln.write(file=os.path.join(cwd, 'out.ali'))



        ref_mdl = model(env, file=ori_pdbfile_names[0])
        atmsel = selection(ref_mdl).only_atom_types('CA')
        mdl2 = model(env, file=ori_pdbfile_names[1])
        r = atmsel.superpose(mdl2, aln)

#        mdl2.write(file=os.path.join(cwd, 'out.pdb'))
        

        translations = [
                        [0.0, 0.0, 0.0], #ref onto ref, kept for compatibility with AllFitter
                        list(r.translation)
                        
                        ]
        rotations_normal = [
                     [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]], #ref onto ref, kept for compatibility with AllFitter
                     list(map(list, r.rotation))
                     ]

        out['transforms'] = {
                             'translation vectors': translations,
                             'rotation matrices': rotations_normal
#                             'rotations angle-axis': rotations_angle_axis
                             }

        out['out_seq_aln'] = get_aln_data(aln)
        out['aln'] = aln


#        saveout = sys.stdout
#        saveerr = sys.stderr
#        sys.stdout = mystdout = StringIO()
#        sys.stderr = mystderr = StringIO()
#        aln.compare_structures(compare_mode=1, output='LONG')
#        sys.stdout = saveout
#        sys.stderr = saveerr
#        
#        stdout_str = mystdout.getvalue()
#        stderr_str = mystderr.getvalue()
#        with open(os.path.join(cwd, 'log.txt'), 'w') as f:
#            f.write(stdout_str)
#        print(cwd)
    finally:
        log.level(*old)
        os.chdir(old_cwd)

    return out


def gen_struct_aln(
                 knowns,
                 cwd=None,
                 atom_files_dirs=None
                 ):

    old = _get_log_level()
    old_cwd = os.getcwd()
    

    try:
        log.very_verbose() #this does not prevent logs from a.make() :-(
        if atom_files_dirs is None:
            atom_files_dirs=['.']
        
        
        if cwd is not None:
            os.chdir(cwd)
            
        env = environ()
        env.io.atom_files_directory = atom_files_dirs
        
        aln = alignment(env)

        for struc in knowns:
            mdl = model(env, file=struc)
            aln.append_model(mdl, atom_files=struc, align_codes=struc)    



#        aln.align3d(gap_penalties_3d=[0, 4.0], output='VERY_LONG')

        aln.salign(
                   gap_penalties_3d=(0, 4.0),

                   feature_weights=(0., 1., 1., 1., 1., 0.),
                   improve_alignment=False,
                   fit=True ,
                   write_fit=True,
                   write_whole_pdb=True,
                   output='ALIGNMENT QUALITY')
        aln.write(file=os.path.join(cwd, 'out.ali'))
    finally:
        log.level(*old)
        os.chdir(old_cwd)

def get_aln_data(aln):
    """Gets aln from modeller aln as:
    [['struct_name1', 'seq1'], ['struct_name2', 'seq2']]
    """
    cols = []
    for pos in aln.positions:
        col = []
        for seq in aln:
            resi = pos.get_residue(seq)
            if not resi:
                resi = '-'
            else:
                resi = resi.code
            col.append(resi)  
        cols.append(col)
    out_seqs = [''.join(seq) for seq in zip(*cols)]
    
    seq_aln = []
    for i, seq in enumerate(aln):
        seq_aln.append([seq.code, out_seqs[i]])
    return seq_aln

def get_aligned_PDB_ranges(pairAln):
    '''
    Note that it returns ranges using PDB resi numbers, which can lead to unpredictable results
    e.g. in PDB files it may happen that first residue has number 900 while second number 1
    '''
    aln = pairAln
    def is_aligned(v):
        if '-' not in v:
            return True
        else:
            return False
    cols = []
    for pos in aln.positions:
        col = []
        for seq in aln:
            resi = pos.get_residue(seq)
            if not resi:
                resi = '-'
            else:
                resi = resi.num
            col.append(resi)  
        cols.append(col)
    
    col_ranges = [list(g) for k, g in groupby(cols, is_aligned)]

    aligned_ranges1 = []
    aligned_ranges2 = []
    for col_range in col_ranges:
        first_col = col_range[0]
        last_col = col_range[-1]
        if '-' not in first_col:
            aligned_ranges1.append([first_col[0], last_col[0]])
            aligned_ranges2.append([first_col[1], last_col[1]])

    return aligned_ranges1, aligned_ranges2

def get_aligned_PDB_resi(pairAln):
    aln = pairAln
    def is_aligned(v):
        if '-' not in v:
            return True
        else:
            return False
    cols = []
    for pos in aln.positions:
        col = []
        for seq in aln:
            resi = pos.get_residue(seq)
            if not resi:
                resi = None
            else:
                resi = resi.num
            col.append(resi)
        if None not in col:
            cols.append(col)
    
            
    return cols

    
def _catch_modeller_logs(fn):
    """
    Run function with re-directing stdout and stderr to logfiles.
    
    Logfiles must be defined by specifying following options of the decorated function:
    o logdir (default: os.getcwd()
    o stdout_filename (default: Modeller.out)
    o stderr_filename (default: Modeller.err)
    
    This is a DECORATOR function.
    """
    #all this stuff is to catch Modeller logs to outfiles
    def wrapper(**kwargs):

        logdir = os.getcwd()
        modeller_outlogfile = os.path.join(logdir, 'Modeller.out')
        modeller_errlogfile = os.path.join(logdir, 'Modeller.err')

        saveout = sys.stdout
        saveerr = sys.stderr
    
        fout = open(modeller_outlogfile, 'w')
        ferr = open(modeller_errlogfile, 'w')
        sys.stdout = fout
        sys.stderr = ferr

        ######
        fn(**kwargs)
        ######
        
        sys.stdout = saveout
        sys.stderr = saveerr
    
        fout.close()
        ferr.close()
        
    return wrapper
    
#    return catch_modeller_logs_decorator


def run_quiet(fn, *args, **kwargs):
    """Run fn catching all stdout and stderr to strings.
    Return:
    o out from fn
    o stdout (string)
    o stderr (string)
    """
    saveout = sys.stdout
    saveerr = sys.stderr
    sys.stdout = mystdout = StringIO()
    sys.stderr = mystderr = StringIO()

    
    ######
    out = fn(*args, **kwargs)
    ######
    sys.stdout = saveout
    sys.stderr = saveerr
    
    stdout_str = mystdout.getvalue()
    stderr_str = mystderr.getvalue()    
    
    return out, stdout_str, stderr_str


#@_catch_modeller_logs
#def test_modeller_load(logdir=None, stdout_filename=None, stderr_filename=None):
#    env = environ()
#    

def test_modeller_load(arg, logdir=None, stdout_filename=None, stderr_filename=None):
    saveout = sys.stdout
    saveerr = sys.stderr
    sys.stdout = mystdout = StringIO()
    sys.stderr = mystderr = StringIO()

    env = environ()
    
    sys.stdout = saveout
    sys.stderr = saveerr
    
    stdout_str = mystdout.getvalue()
    stderr_str = mystderr.getvalue()


    
