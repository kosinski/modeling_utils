"""
Validate modeller key in a given python installation and environment.
"""
import os
import sys
import traceback

def validate_key(modeller_key,
                 modeller_version, #e.g. 9v8
                 ):
    """
    /home/kosa/tmp/test/bin/python validate_key.py MODELIRANJE 9v9 > /dev/null
    
    Messages are printed to STDERR because modeller C lib prints to STDOUT,
    and printing the messages was the only way I found to separate the streams.
    """
    
    modeller_env_key = 'KEY_MODELLER' + modeller_version
    os.environ[modeller_env_key] = modeller_key
    
    try:
        import modeller
    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        if exc_value.message.startswith('check_lice_E> The license key is not set'):
            sys.stderr.write('You must provide a Modeller key\n')
        elif exc_value.message.startswith('check_lice_E> Invalid license key'):
            sys.stderr.write('Modeller license key is invalid\n')
        else:
            print "Unexpected error: ", exc_traceback
            raise
    else:
        sys.stderr.write('OK\n')
if __name__ == '__main__':
    validate_key(
                 modeller_key=sys.argv[1],
                 modeller_version=sys.argv[2]
                                  )