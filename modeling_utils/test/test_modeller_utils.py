import os
import sys
import shutil
import unittest
import tempfile

from modeling_utils import modeller_utils

#os.environ['KEY_MODELLER9v8'] = 'MODELIRANJE'
##from modeller import log
##from PyPlatform.utils import unittest_ext
#import traceback
#import _modeller
#try:
#    from PyPlatform.modeling import modeller_utils
#except _modeller.ModellerError as e:
#    if e.message.startswith('check_lice_E> The license key is not set'):
#        print('You must provide a Modeller key')
#    elif e.message.startswith('check_lice_E> Invalid license key'):
#        print('Provided Modeller license key is invalid')
#    else:
#        print("Unexpected error: ", traceback.format_exc())
#else:
#    pass
#    #do the code
class MyTestCase(unittest.TestCase):
    def check_remove_tails_inserts(self, aln, out, msg=None):
        target_ori = aln[0][1].replace('-','')
        target_new = []
        out_aln = out[0]
        ori_map = out[1]

        target_new = out_aln[0][1].replace('-','').replace('/','').replace('$','')
        target_new_from_ori_map = ''
        for i in ori_map:
            target_new_from_ori_map += target_ori[i]

        self.assertEqual(target_new, target_new_from_ori_map)
        
        
class test_modeller_utils(MyTestCase):
    def setUp(self):
        self.cur_dir = os.getcwd()
        self.test_data = os.path.join(os.getcwd(), 'test_modeller_utils_data')
        self.fasta_filename = os.path.join(self.test_data, 'aln.fasta')
        self.fasta_file = open(self.fasta_filename)
        self.pir_outfilename = 'TODO'
        self.pir_single_template = os.path.join(self.test_data, 'aln.SINGLE_TEMPLATE.pir')
        
        self.pir_multi_template = os.path.join(self.test_data, 'aln.MULTI_TEMPLATE.pir')
        
        self.clean_pdbfilename = '3mhtA.pdb'
        
        self.aln_SINGLE_TEMPLATE = [
                ['TARGET', '--------------MKIWSKEEVVNK--------LHEIKNKGYLSVPTDMF\
RTDDGVVGQILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYVKPSSRNPEVM\
KKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIEKLVLVFAETIGRANSPEEQFHFTKAYMLT\
----EI-------NDITSLINDGVLVMDLCIDQDLSKSKGPHDRGPHLRIPISKLDKLYRNIERLL-------------\
-------------------------------------------------------------------------------\
---------------------------------------------------------'],
                ['2odi_A', '--------------MKIWSKEEVVNK--------LHEIKNKGYLSVPTDMF\
RTDDGVVGQILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYVKPSSRNPEVM\
KKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIEKLVLVFAETIGRANSPEEQFHFTKAYMLT\
----EI-------NDITSLINDGVLVMDLCIDQDLSKSKGPHDRGPHLRIPISKLDKLYRNIERLL-------------\
-------------------------------------------------------------------------------\
---------------------------------------------------------']
                ]

        self.aln_MULTI_TEMPLATE = [
                ['TARGET', '--------------MKIWSKEEVVNK--------LHEIKNKGYLSVPTDMFRTDDGVVGQ\
ILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYV\
KPSSRNPEVMKKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIE\
KLVLVFAETIGRANSPEEQFHFTKAYMLT----EI-------NDITSLINDGVLVMDLCI\
DQDLSKSKGPHDRGPHLRIPISKLDKLYRNIERLL-------------------------\
------------------------------------------------------------\
------------------------------------------------------------\
----'],
                ['2odi_A', '--------------MKIWSKEEVVNK--------LHEIKNKGYLSVPTDMFRTDDGVVGQ\
ILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYV\
KPSSRNPEVMKKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIE\
KLVLVFAETIGRANSPEEQFHFTKAYMLT----EI-------NDITSLINDGVLVMDLCI\
DQDLSKSKGPHDRGPHLRIPISKLDKLYRNIERLL-------------------------\
------------------------------------------------------------\
------------------------------------------------------------\
----'],
                ['2aor_A', '------------MI--PQTLEQLLSQAQSIAGLTFGELADELHIPVPIDL-KRDKGWVGM\
LLERALGATAGSKAEQDFSHLGVELKTLP-------------------------------\
------------------------------------------------------------\
------------------------------------------------------------\
-----------------------------------INAEGYPLETTFVSLAPLVQNSGVK\
WENSHVRHKLSCVLWMPIEGSRHIPLRERHIGAPIFWKPTAEQERQLKQDWEELMDLIVL\
GKLDQITARIGEVMQLRPKGANSRAVTKGIGKNGEIIDTLPLGFYLRKEFTAQILNAFLE\
T---'],
                ['1azo_A', '------PRPLLSPP---ETEEQLLAQAQQLSGYTLGELAALVGLVTPENL-KRDKGWIGV\
LLEIWLGA-----PEQDFAALGVELKTIP-VDSLGR------------------------\
------PLE--TTFVCVA------------P----------LTGNSGVTWETSHVRHKLK\
RVLWIPVEGE--ASIPLAQRRVGSPLLWSPNEEEDRQLREDWEELMDMIVLGQV------\
-----------------------------------ERITARHGEYLQIRP-------LTE\
AIGARGERILTLPRGFYLKKNFTSALLARHFLIQ--------------------------\
------------------------------------------------------------\
----']
                ]

        self.tempdir = None
        
    def tearDown(self):
        os.chdir(self.cur_dir)
        self.fasta_file.close()
        if self.tempdir:
            pass
            shutil.rmtree(self.tempdir)
        
#    @unittest.skip('')
    def test_create_modeller_pir_from_fasta(self):
        
        pir = modeller_utils.create_modeller_pir_from_fasta(self.fasta_file)
        pass
        #self.assertEqual(a, b)
        #self.assertFilesEqual(self.outfilename, self.expected_output_filename)

#    @unittest.skip('')
    def test_create_modeller_pir_SINGLE_TEMPLATE(self):
        pir, num_map, alnobj = modeller_utils.create_modeller_pir(self.aln_SINGLE_TEMPLATE, chain_config={'2odi_A': 'A'})
        

#    @unittest.skip('')
    def test_create_modeller_pir_MULTI_TEMPLATE(self):



        pir, num_map, alnobj = modeller_utils.create_modeller_pir(self.aln_MULTI_TEMPLATE,
                                                 chain_config={
                                                               '2odi_A': 'A',
                                                               '2aor_A': 'A',
                                                               '1azo_A': 'A',
                                                               }
                                                 )
        print(pir)

    def test_remove_tails_inserts(self):

        
        alndata = [
                ['TARGET', 'ABCD-EFGHIJ'],
                ['2odi_A', '--CDQE-G---'],
                ['2aor_A', '-BCDQE-G---'],
                ['1azo_A', '---DQE-G---']
                ]

        gold_aln = [
                ['TARGET', 'BCD-E$G'],
                ['2odi_A', '-CDQE-G'],
                ['2aor_A', 'BCDQE-G'],
                ['1azo_A', '--DQE-G']
                ]


        aln = modeller_utils.Aln(alndata)
        aln.remove_tails_inserts(
                                         skip_tails=True,
                                         skip_inserts=True                                           
                                           )

        self.check_remove_tails_inserts(alndata, (aln.aln, aln.ori_new_mapping))
        self.assertEqual(aln.aln, gold_aln)

    def test_remove_tails_inserts_longer_than(self):

        
        alndata = [
                ['TARGET', 'ABCD-EFXXGHIJ'],
                ['2odi_A', '--CDQE---G---'],
                ['2aor_A', '-BCDQE---G---'],
                ['1azo_A', '---DQE---G---']
                ]

        gold_aln = [
                ['TARGET', 'ABCD-EF$XGH'],
                ['2odi_A', '--CDQE---G-'],
                ['2aor_A', '-BCDQE---G-'],
                ['1azo_A', '---DQE---G-']
                ]


        aln = modeller_utils.Aln(alndata)
        aln.remove_tails_inserts(
                                         skip_tails=True,
                                         skip_inserts=True,
                                         flanks=1                                    
                                           )

        self.check_remove_tails_inserts(alndata, (aln.aln, aln.ori_new_mapping))
        self.assertEqual(aln.aln, gold_aln)


    def test_remove_tails_inserts_inserts_only(self):

        
        alndata = [
                ['TARGET', 'ABCD-EFGHIJ'],
                ['2odi_A', '--CDQE-G---'],
                ['2aor_A', '-BCDQE-G---'],
                ['1azo_A', '---DQE-G---']
                ]

        gold_aln = [
                ['TARGET', 'ABCD-E$GHIJ'],
                ['2odi_A', '--CDQE-G---'],
                ['2aor_A', '-BCDQE-G---'],
                ['1azo_A', '---DQE-G---']
                ]

        aln = modeller_utils.Aln(alndata)
        aln.remove_tails_inserts(
                                         skip_tails=False,
                                         skip_inserts=True                                           
                                           )

        self.check_remove_tails_inserts(alndata, (aln.aln, aln.ori_new_mapping))
        self.assertEqual(aln.aln, gold_aln)

        
    def test_remove_tails_inserts_tails_only(self):
        """Should remove tails when skip_tails=True and skip_inserts=False"""
        
        alndata = [
                ['TARGET', '-ABCD-EFGHIJ--'],
                ['2odi_A', 'X--CDQE-G---YY'],
                ['2aor_A', 'X-BCDQE-G---YY'],
                ['1azo_A', 'X---DQE-G---YY']
                ]

        gold_aln = [
                ['TARGET', '-BCD-EFG--'],
                ['2odi_A', 'X-CDQE-GYY'],
                ['2aor_A', 'XBCDQE-GYY'],
                ['1azo_A', 'X--DQE-GYY']
                ]


        aln = modeller_utils.Aln(alndata)
        aln.remove_tails_inserts(
                                         skip_tails=True,
                                         skip_inserts=False                                           
                                           )
        
         
        self.check_remove_tails_inserts(alndata, (aln.aln, aln.ori_new_mapping))
        self.assertEqual(aln.aln, gold_aln)

    def test_remove_tails_inserts_real(self):

        alndata = [['TARGET', '------------------------KTMFAEME-IIGQFNLGFIITKLNEDIFIVDQHATDEKYNFEMLQQHT----VLQGQRLIAPQTLNLTAVNEAVLIENLEIFRKNGFDFVIDENAPVTERAKLISLPTSKNWTFGPQDVDELIFMLSDSPGVMCRPSRVKQMFASRACRKSVMIGTALNTSEMKKLITHMGEMDHPWNCPHGRPTMRHIANLGVISQN-----------'],
               ['3kdg_A_cleaned', '-----------------------M-DRVPIMY-PIGQMHGTYILAQNENGLYIIDQHAAQERIKYEYFREKVGEV-EPEVQEMIVPLTFHYSTNEALIIEQHKQELESVGVFLESFGS----NSYIVRCHPAWFPKGEEAELIEEIIQQVLDSKN-IDIKKLREEAAIMMSCKG----NRHLRNDEIKALLDDLRSTSDPFTCPHGRPIIIHHST-------YEMEKMFKRVM']]    

        
        gold_aln = [['TARGET', '------------------------TMFAEME-IIGQFNLGFIITKLNEDIFIVDQHATDEKYNFEMLQQHT----VLQGQRLIAPQTLNLTAVNEAVLIENLEIFRKNGFDFVIDENAPVTERAKLISLPTSKNWTFGPQDVDELIFMLSDSPGVMCRPSRVKQMFASRACRKSVMIGTALNTSEMKKLITHMGEMDHPWNCPHGRPTMRHIAN-----------'],
                    ['3kdg_A_cleaned', '-----------------------MDRVPIMY-PIGQMHGTYILAQNENGLYIIDQHAAQERIKYEYFREKVGEV-EPEVQEMIVPLTFHYSTNEALIIEQHKQELESVGVFLESFGS----NSYIVRCHPAWFPKGEEAELIEEIIQQVLDSKN-IDIKKLREEAAIMMSCKG----NRHLRNDEIKALLDDLRSTSDPFTCPHGRPIIIHHSTYEMEKMFKRVM']]
        aln = modeller_utils.Aln(alndata)
        aln.remove_tails_inserts(
                                         skip_tails=True,
                                         skip_inserts=False                                           
                                           )

        self.assertEqual(aln.aln, gold_aln)

    def test_remove_tails_inserts_real_longer_than(self):

        alndata = [['TARGET', '------------------------KTMFAEME-IIGQFNLGFIITKLNEDIFIVDQHATDEKYNFEMLQQHT----VLQGQRLIAPQTLNLTAVNEAVLIENLEIFRKNGFDFVIDENAPVTERAKLISLPTSKNWTFGPQDVDELIFMLSDSPGVMCRPSRVKQMFASRACRKSVMIGTALNTSEMKKLITHMGEMDHPWNCPHGRPTMRHIANLGVISQN-----------'],
               ['3kdg_A_cleaned', '-----------------------M-DRVPIMY-PIGQMHGTYILAQNENGLYIIDQHAAQERIKYEYFREKVGEV-EPEVQEMIVPLTFHYSTNEALIIEQHKQELESVGVFLESFGS----NSYIVRCHPAWFPKGEEAELIEEIIQQVLDSKN-IDIKKLREEAAIMMSCKG----NRHLRNDEIKALLDDLRSTSDPFTCPHGRPIIIHHST-------YEMEKMFKRVM']]    

        
        gold_aln = [['TARGET', '------------------------KTMFAEME-IIGQFNLGFIITKLNEDIFIVDQHATDEKYNFEMLQQHT----VLQGQRLIAPQTLNLTAVNEAVLIENLEIFRKNGFDFVIDENAPVTERAKLISLPTSKNWTFGPQDVDELIFMLSDSPGVMCRPSRVKQMFASRACRKSVMIGTALNTSEMKKLITHMGEMDHPWNCPHGRPTMRHIANLG-----------'],
               ['3kdg_A_cleaned', '-----------------------M-DRVPIMY-PIGQMHGTYILAQNENGLYIIDQHAAQERIKYEYFREKVGEV-EPEVQEMIVPLTFHYSTNEALIIEQHKQELESVGVFLESFGS----NSYIVRCHPAWFPKGEEAELIEEIIQQVLDSKN-IDIKKLREEAAIMMSCKG----NRHLRNDEIKALLDDLRSTSDPFTCPHGRPIIIHHST--YEMEKMFKRVM']]    

        aln = modeller_utils.Aln(alndata)
        aln.remove_tails_inserts(
                                         skip_tails=True,
                                         skip_inserts=False,
                                         flanks=2

                                           )

        self.assertEqual(aln.aln, gold_aln)


    def test_remove_tails_inserts_missing(self):

        
        alndata = [
                ['TARGET', 'ABCD-EFGHIJ'],
                ['2odi_A', 'ABCD-EfghIJ'],
                ]

        gold_aln = [
                ['TARGET', 'ABCD-E$IJ'],
                ['2odi_A', 'ABCD-E-IJ'],
                ]
        aln = modeller_utils.Aln(alndata)
        aln.remove_tails_inserts(
                                         skip_tails=True,
                                         skip_inserts=True,
                                         skip_missing=True                                         
                                           )

        self.check_remove_tails_inserts(alndata, (aln.aln, aln.ori_new_mapping))
        self.assertEqual(aln.aln, gold_aln)


    def test_remove_tails_inserts_missing1(self):

        
        alndata = [
                ['TARGET', 'ABCD-E-FG-H-IJ'],
                ['2odi_A', 'ABCD-E--g-h-iJ'],
                ['2aor_A', 'ABCD-E-fG-H-iJ'],
                ]

        gold_aln = [
                ['TARGET', 'ABCD-E-$G-H-$J'],
                ['2odi_A', 'ABCD-E-------J'],
                ['2aor_A', 'ABCD-E--G-H--J'],
                ]
        aln = modeller_utils.Aln(alndata)
        aln.remove_tails_inserts(
                                         skip_tails=True,
                                         skip_inserts=True,
                                         skip_missing=True                                         
                                           )
        
        self.check_remove_tails_inserts(alndata, (aln.aln, aln.ori_new_mapping))
        self.assertEqual(aln.aln, gold_aln)



    def test_remove_tails_inserts_1(self):

        
        alndata = [
                ['TARGET', '-----------------------------------MRLPNGYWLYNRGEDTLMLDLGRMHSVVVSERRKQ--KKE-TGVGQKLLFSIEYLLNEMEKNKYRSIKKYLPQLGFEMELGDD----NVLRINAVPNGLKEKQVNEFLEKLFEVLDYKT----EEEFMAYYENQWTKINAKSRFDFFYKTEVEQMIKDFSDLGFPEYTPSGKKCYIALPL-------EDLKNKIK---'],
                ['2xxx_A', '-----------------------M-DRVPIMY-PIGQMHGTYILAQNENGLYIIDQHAAQERIKYEYFREKVGEV-EPEVQEMIVPLTFHYSTNEALIIEQHKQELESVGVFLESFGS----NSYIVRCHPAWFPKGEEAELIEEIIQQVLDSKN-IDIKKLREEAAIMMSCKG----NRHLRNDEIKALLDDLRSTSDPFTCPHGRPIIIHHST-------YEMEKMFKRVM'],
                ]


        aln = modeller_utils.Aln(alndata)
        aln.remove_tails_inserts(
                                         skip_tails=True,
                                         skip_inserts=True                                           
                                           )
        
        self.check_remove_tails_inserts(alndata, (aln.aln, aln.ori_new_mapping))
        
        target_degapped = aln.aln[0][1].replace('-','').replace('$','')
        print(len(target_degapped))
        print(len(aln.ori_new_mapping))

    def test_create_modeller_pir_noInserts_MULTI_TEMPLATE(self):
        aln = [
                ['TARGET', 'ABCDEFGHIJ'],
                ['2odi_A', '--CDE-G---'],
                ['2aor_A', '-BCDE-G---'],
                ['1azo_A', '---DE-G---']
                ]

        #remove inserts only
        out_pir = """>P1;TARGET
sequence:TARGET: : : : : : : :
ABCDE/GHIJ*
>P1;2odi_A
structure:2odi_A: :A: :A: : : :
--CDE-G---*
>P1;2aor_A
structure:2aor_A: :A: :A: : : :
-BCDE-G---*
>P1;1azo_A
structure:1azo_A: :A: :A: : : :
---DE-G---*"""
        pir = modeller_utils.create_modeller_pir(aln,
                                                 chain_config={
                                                               '2odi_A': 'A',
                                                               '2aor_A': 'A',
                                                               '1azo_A': 'A',
                                                               },
                                                 skip_tails=False,
                                                 skip_inserts=True
                                                 )
        self.assertEqual(pir[0], out_pir)

        #remove tails only
        out_pir = """>P1;TARGET
sequence:TARGET: : : : : : : :
BCDEFG*
>P1;2odi_A
structure:2odi_A: :A: :A: : : :
-CDE-G*
>P1;2aor_A
structure:2aor_A: :A: :A: : : :
BCDE-G*
>P1;1azo_A
structure:1azo_A: :A: :A: : : :
--DE-G*"""
        pir = modeller_utils.create_modeller_pir(aln,
                                                 chain_config={
                                                               '2odi_A': 'A',
                                                               '2aor_A': 'A',
                                                               '1azo_A': 'A',
                                                               },
                                                 skip_tails=True,
                                                 skip_inserts=False
                                                 )
        self.assertEqual(pir[0], out_pir)

        #remove both tails and inserts
        out_pir = """>P1;TARGET
sequence:TARGET: : : : : : : :
BCDE/G*
>P1;2odi_A
structure:2odi_A: :A: :A: : : :
-CDE-G*
>P1;2aor_A
structure:2aor_A: :A: :A: : : :
BCDE-G*
>P1;1azo_A
structure:1azo_A: :A: :A: : : :
--DE-G*"""

        pir = modeller_utils.create_modeller_pir(aln,
                                                 chain_config={
                                                               '2odi_A': 'A',
                                                               '2aor_A': 'A',
                                                               '1azo_A': 'A',
                                                               },
                                                 skip_tails=True,
                                                 skip_inserts=True
                                                 )
        self.assertEqual(pir[0], out_pir)
        
 
        #do not remove anything
        out_pir = """>P1;TARGET
sequence:TARGET: : : : : : : :
ABCDEFGHIJ*
>P1;2odi_A
structure:2odi_A: :A: :A: : : :
--CDE-G---*
>P1;2aor_A
structure:2aor_A: :A: :A: : : :
-BCDE-G---*
>P1;1azo_A
structure:1azo_A: :A: :A: : : :
---DE-G---*"""
        pir = modeller_utils.create_modeller_pir(aln,
                                                 chain_config={
                                                               '2odi_A': 'A',
                                                               '2aor_A': 'A',
                                                               '1azo_A': 'A',
                                                               },
                                                 skip_tails=False,
                                                 skip_inserts=False
                                                 )
        self.assertEqual(pir[0], out_pir)


    def test_remove_tails_inserts_on2reu_A(self):

        
        alndata = [
                ['TARGET', 'ABCD-EFGHIJ'],
                ['2odi_A', '--CDQE-G---'],
                ['2aor_A', '-BCDQE-G---'],
                ['1azo_A', '---DQE-G---']
                ]



        aln = [
               ['TARGET','-------------------------------------MKIWSKEEVVNKLHEIKNKGYLSVPTDMFRTDDGVVGQILERQFGVQENN------ITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYVKPSSRNPEVMKKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLNLSKIEKLVLVFAETIGRANSPEEQFHFTKAYMLTEINDITSLINDGVLVMDLCIDQDLSKSKGPHDRGPHLRIPISKLDKLYRNIERLL------------------------------------------------------------------------------------------------------------------------------------------------------------------------------'],
               ['2reu_A', 'MKEKSIEDIVFEKFQPYINWSIDKLCEHFSINKGEKG----------------------------------LNYRIASAILNLKGKTTKSKPFPEVEEFEKSSIVVKTVH-FNKKNVNK--------------------------------------------------------------------------------------------------------------------------------------------------------------------ESMSFGAFKFEELANEEWEDSEGYPSAQWRNFLLETRFLFFVVKEDEDGVDIFKGIKFFSMPEEDINGPVKRMWDDTVKKLKEGVTLEAVPDKSTKDGWRIKNNFVDKSDDLICHVRPHTNNRDYRGGSNADKLPKKINWINRPDSDDYSDEWMTKQSFWINNDYIKKQVEDLL']
               ]

        aln = modeller_utils.Aln(alndata)
        aln.remove_tails_inserts(
                                         skip_tails=True,
                                         skip_inserts=True                                           
                                           )

    def test_create_modeller_pir_include_regions(self):
        aln = self.aln_MULTI_TEMPLATE

        out_pir_full = """>P1;TARGET\nsequence:TARGET: : : : : : : :\n\
--------------MKIWSKEEVVNK--------LHEIKNKGYLSVPTDMFRTDDGVVGQILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYVKPSSRNPEVMKKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIEKLVLVFAETIGRANSPEEQFHFTKAYMLT----EI-------NDITSLINDGVLVMDLCIDQDLSKSKGPHDRGPHLRIPISKLDKLYRNIERLL-----------------------------------------------------------------------------------------------------------------------------------------------------*\n\
>P1;2odi_A\nstructure:2odi_A: :A: :A: : : :\n\
--------------MKIWSKEEVVNK--------LHEIKNKGYLSVPTDMFRTDDGVVGQILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYVKPSSRNPEVMKKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIEKLVLVFAETIGRANSPEEQFHFTKAYMLT----EI-------NDITSLINDGVLVMDLCIDQDLSKSKGPHDRGPHLRIPISKLDKLYRNIERLL-----------------------------------------------------------------------------------------------------------------------------------------------------*\n\
>P1;2aor_A\nstructure:2aor_A: :A: :A: : : :\n\
------------MI--PQTLEQLLSQAQSIAGLTFGELADELHIPVPIDL-KRDKGWVGMLLERALGATAGSKAEQDFSHLGVELKTLP------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------INAEGYPLETTFVSLAPLVQNSGVKWENSHVRHKLSCVLWMPIEGSRHIPLRERHIGAPIFWKPTAEQERQLKQDWEELMDLIVLGKLDQITARIGEVMQLRPKGANSRAVTKGIGKNGEIIDTLPLGFYLRKEFTAQILNAFLET---*\n\
>P1;1azo_A\nstructure:1azo_A: :A: :A: : : :\n\
------PRPLLSPP---ETEEQLLAQAQQLSGYTLGELAALVGLVTPENL-KRDKGWIGVLLEIWLGA-----PEQDFAALGVELKTIP-VDSLGR------------------------------PLE--TTFVCVA------------P----------LTGNSGVTWETSHVRHKLKRVLWIPVEGE--ASIPLAQRRVGSPLLWSPNEEEDRQLREDWEELMDMIVLGQV-----------------------------------------ERITARHGEYLQIRP-------LTEAIGARGERILTLPRGFYLKKNFTSALLARHFLIQ------------------------------------------------------------------------------------------*"""

        out_pir = """>P1;TARGET\nsequence:TARGET: : : : : : : :\n\
-------------\
-MKIWSKEEVVN\
---------\
K--------LHEIKNKG--------/RTDDGVVGQILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYVKPSSRNPEVMKKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIEKLVLVFAETIGRANSPEEQFHFTKAYMLT----EI-------NDITSLINDGVLVMDLCIDQDLSKSKGPHDRGP--------------\
------\
------\
-----------------------------------------------------------------------------------------------------------------------------------------------------*\n\
>P1;2odi_A\nstructure:2odi_A: :A: :A: : : :\n\
-------------\
-MKIWSKEEVVN\
---------\
K--------LHEIKNKGYLSVPTDMFRTDDGVVGQILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYVKPSSRNPEVMKKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIEKLVLVFAETIGRANSPEEQFHFTKAYMLT----EI-------NDITSLINDGVLVMDLCIDQDLSKSKGPHDRGPHLRIPISKLDKLYR\
------\
NIERLL\
-----------------------------------------------------------------------------------------------------------------------------------------------------*\n\
>P1;2aor_A\nstructure:2aor_A: :A: :A: : : :\n\
------------M\
I--PQTLEQLLS\
---------\
QAQSIAGLTFGELADELHIPVPIDL-KRDKGWVGMLLERALGATAGSKAEQDFSHLGVELKTLP------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\
------\
------\
INAEGYPLETTFVSLAPLVQNSGVKWENSHVRHKLSCVLWMPIEGSRHIPLRERHIGAPIFWKPTAEQERQLKQDWEELMDLIVLGKLDQITARIGEVMQLRPKGANSRAVTKGIGKNGEIIDTLPLGFYLRKEFTAQILNAFLET---*\n\
>P1;1azo_A\nstructure:1azo_A: :A: :A: : : :\n\
------PRPLLSP\
------------\
PETEEQLLA\
QAQQLSGYTLGELAALVGLVTPENL-KRDKGWIGVLLEIWLGA-----PEQDFAALGVELKTIP-VDSLGR------------------------------PLE--TTFVCVA------------P----------LTGNSGVTWETSHVRHKLKRVLWIPVEGE--ASIPLAQRRVGSPLLWSPNEEEDRQLREDWEELMDMIVLGQV-----------------------------------\
------\
------\
ERITARHGEYLQIRP-------LTEAIGARGERILTLPRGFYLKKNFTSALLARHFLIQ------------------------------------------------------------------------------------------*"""



        include_regions = [
            ['TARGET', ((1,20), (30,218))],
            ['2odi_A', ((1,232),)],
            ['2aor_A', ((1,100000),)],
            ['1azo_A', ((1,10), (20,10000))]
        ]

        os.chdir(self.test_data)
        pir, num_map, alnobj = modeller_utils.create_modeller_pir(aln,
                                                 chain_config={
                                                               '2odi_A': 'A',
                                                               '2aor_A': 'A',
                                                               '1azo_A': 'A',
                                                               },
                                                 include_regions=include_regions
                                                 )
        for line1, line2 in zip(pir.splitlines(), out_pir.splitlines()):
            if line1.startswith('>'):
                print(line1, line2)
            print(line1)
            print(line2)
            self.assertEqual(line1, line2)


    def test_create_modeller_pir_include_regions_target(self):
        aln = self.aln_MULTI_TEMPLATE

        out_pir_full = """>P1;TARGET\nsequence:TARGET: : : : : : : :\n\
--------------MKIWSKEEVVNK--------LHEIKNKGYLSVPTDMFRTDDGVVGQILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYVKPSSRNPEVMKKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIEKLVLVFAETIGRANSPEEQFHFTKAYMLT----EI-------NDITSLINDGVLVMDLCIDQDLSKSKGPHDRGPHLRIPISKLDKLYRNIERLL-----------------------------------------------------------------------------------------------------------------------------------------------------*\n\
>P1;2odi_A\nstructure:2odi_A: :A: :A: : : :\n\
--------------MKIWSKEEVVNK--------LHEIKNKGYLSVPTDMFRTDDGVVGQILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYVKPSSRNPEVMKKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIEKLVLVFAETIGRANSPEEQFHFTKAYMLT----EI-------NDITSLINDGVLVMDLCIDQDLSKSKGPHDRGPHLRIPISKLDKLYRNIERLL-----------------------------------------------------------------------------------------------------------------------------------------------------*\n\
>P1;2aor_A\nstructure:2aor_A: :A: :A: : : :\n\
------------MI--PQTLEQLLSQAQSIAGLTFGELADELHIPVPIDL-KRDKGWVGMLLERALGATAGSKAEQDFSHLGVELKTLP------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------INAEGYPLETTFVSLAPLVQNSGVKWENSHVRHKLSCVLWMPIEGSRHIPLRERHIGAPIFWKPTAEQERQLKQDWEELMDLIVLGKLDQITARIGEVMQLRPKGANSRAVTKGIGKNGEIIDTLPLGFYLRKEFTAQILNAFLET---*\n\
>P1;1azo_A\nstructure:1azo_A: :A: :A: : : :\n\
------PRPLLSPP---ETEEQLLAQAQQLSGYTLGELAALVGLVTPENL-KRDKGWIGVLLEIWLGA-----PEQDFAALGVELKTIP-VDSLGR------------------------------PLE--TTFVCVA------------P----------LTGNSGVTWETSHVRHKLKRVLWIPVEGE--ASIPLAQRRVGSPLLWSPNEEEDRQLREDWEELMDMIVLGQV-----------------------------------------ERITARHGEYLQIRP-------LTEAIGARGERILTLPRGFYLKKNFTSALLARHFLIQ------------------------------------------------------------------------------------------*"""

        out_pir = """>P1;TARGET\nsequence:TARGET: : : : : : : :\n\
--------------MKIWSKEEVVNK--------LHEIKNKG--------/RTDDGVVGQILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYVKPSSRNPEVMKKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIEKLVLVFAETIGRANSPEEQFHFTKAYMLT----EI-------NDITSLINDGVLVMDLCIDQDLSKSKGPHDRGP-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*\n\
>P1;2odi_A\nstructure:2odi_A: :A: :A: : : :\n\
--------------MKIWSKEEVVNK--------LHEIKNKGYLSVPTDMFRTDDGVVGQILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYVKPSSRNPEVMKKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIEKLVLVFAETIGRANSPEEQFHFTKAYMLT----EI-------NDITSLINDGVLVMDLCIDQDLSKSKGPHDRGPHLRIPISKLDKLYRNIERLL-----------------------------------------------------------------------------------------------------------------------------------------------------*\n\
>P1;2aor_A\nstructure:2aor_A: :A: :A: : : :\n\
------------MI--PQTLEQLLSQAQSIAGLTFGELADELHIPVPIDL-KRDKGWVGMLLERALGATAGSKAEQDFSHLGVELKTLP------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------INAEGYPLETTFVSLAPLVQNSGVKWENSHVRHKLSCVLWMPIEGSRHIPLRERHIGAPIFWKPTAEQERQLKQDWEELMDLIVLGKLDQITARIGEVMQLRPKGANSRAVTKGIGKNGEIIDTLPLGFYLRKEFTAQILNAFLET---*\n\
>P1;1azo_A\nstructure:1azo_A: :A: :A: : : :\n\
------PRPLLSPP---ETEEQLLAQAQQLSGYTLGELAALVGLVTPENL-KRDKGWIGVLLEIWLGA-----PEQDFAALGVELKTIP-VDSLGR------------------------------PLE--TTFVCVA------------P----------LTGNSGVTWETSHVRHKLKRVLWIPVEGE--ASIPLAQRRVGSPLLWSPNEEEDRQLREDWEELMDMIVLGQV-----------------------------------------ERITARHGEYLQIRP-------LTEAIGARGERILTLPRGFYLKKNFTSALLARHFLIQ------------------------------------------------------------------------------------------*"""


        out_num_map = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217]

        include_regions = [
            ['TARGET', ((1,20), (30,218))],
            ['2odi_A', ((1,100000),)],
            ['2aor_A', ((1,100000),)],
            ['1azo_A', ((1,100000),)]            
        ]

        os.chdir(self.test_data)
        pir, num_map, alnobj = modeller_utils.create_modeller_pir(aln,
                                                 chain_config={
                                                               '2odi_A': 'A',
                                                               '2aor_A': 'A',
                                                               '1azo_A': 'A',
                                                               },
                                                 include_regions=include_regions
                                                 )
        # self.assertEqual(pir, out_pir)

        for line1, line2 in zip(pir.splitlines(), out_pir.splitlines()):
            if line1.startswith('>'):
                print(line1, line2)
            self.assertEqual(line1, line2)

        self.assertEqual(num_map, out_num_map)

                
#    @unittest.skip('')
    def test_load_modeller(self):
        """Should not give errors"""
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        modeller_utils.test_modeller_load(
                                          'blah',
                                          logdir=self.tempdir,
                                          #tempdir,
                                          stdout_filename='test_load.out',
                                          stderr_filename='test_load_err'
                                          )

#    @unittest.skip('')
    def test_run_modeller_SINGLE_TEMPLATE(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        modeller_utils.run_modeller(alnfile=self.pir_single_template,
                                    knowns=('2odi_A',),
                                    sequence='TARGET',
                                    cwd=self.tempdir,
                                    atom_files_dirs=[self.test_data],
                                    very_fast=True,
                                    starting_models=1,
                                    ending_models=1)

#    @unittest.skip('')
    def test_run_modeller_MULTI_TEMPLATE(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        modeller_utils.run_modeller(alnfile=self.pir_multi_template,
                                    knowns=('2odi_A', '2aor_A', '1azo_A'),
                                    sequence='TARGET',
                                    cwd=self.tempdir,
                                    atom_files_dirs=[self.test_data],
                                    very_fast=True,
                                    starting_models=1,
                                    ending_models=1)

    def test_run_modeller_test_break(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        modeller_utils.run_modeller(alnfile=os.path.join(self.test_data, 'aln.test_break.pir'),
                                    knowns=('3kdg_A',),
                                    sequence='TARGET',
                                    cwd=self.tempdir,
                                    atom_files_dirs=[self.test_data],
                                    very_fast=True,
                                    starting_models=1,
                                    ending_models=1,
                                    with_breaks=True)


    def test_run_modeller_from_aln_data_test_break(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        aln_data = [[
                     'TARGET',
                     'MDRVPIMYPI///G//QMHGTYILAQNENGLYIIDQHAAQERIKYEYFREKVGEVEPEVQEMIVPLTFHYSTNEALIIEQHKQELESVGVFLESFGSNSYIVRCHPAWFPKGEEAELIEEIIQQVLDSKNIDIKKLREEAAIMMSCKG/NRHLRNDEIKALLDDLRSTSDPFTCPHGRPIIIHHSTYEMEKMFKRVM'
                     ],
                    [
                     '3kdg_A',
                     'MDRVPIMYPI---G--QMHGTYILAQNENGLYIIDQHAAQERIKYEYFREKVGEVEPEVQEMIVPLTFHYSTNEALIIEQHKQELESVGVFLESFGSNSYIVRCHPAWFPKGEEAELIEEIIQQVLDSKNIDIKKLREEAAIMMSCKG-NRHLRNDEIKALLDDLRSTSDPFTCPHGRPIIIHHSTYEMEKMFKRVM'
                    ]
                    ]
        modeller_utils.run_modeller_from_aln_data(aln_data,
                                    knowns=('3kdg_A',),
                                    sequence='TARGET',
                                    cwd=self.tempdir,
                                    atom_files_dirs=[self.test_data],
                                    very_fast=True,
                                    starting_models=1,
                                    ending_models=1,
                                    with_breaks=True)        


    def test_run_modeller_pepcomposer(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        aln_data = [
                    ['21_93_5_23_3o8b_A_0_781', 
                     'APITAYSQQTRGLLGCIITSLTGRDKNQVDGEVQVLSTATQSFLATCVNGVCWTVYHGAGSKTLAGPKGPITQMYTNVDQDLVGWPAPPGARSMTPCTCGSSDLYLVTRHADVIPVRRRGDSRGSLLSPRPVSYLKGSSGGPLLCPSGHVVGIFRAAVCTRGVAKAVDFIPVESM/AAVMS'],
                    ['J5UAW_receptor',
                     'APITAYSQQTRGLLGCIITSLTGRDKNQVDGEVQVLSTATQSFLATCVNGVCWTVYHGAGSKTLAGPKGPITQMYTNVDQDLVGWPAPPGARSMTPCTCGSSDLYLVTRHADVIPVRRRGDSRGSLLSPRPVSYLKGSSGGPLLCPSGHVVGIFRAAVCTRGVAKAVDFIPVESM/-----'],
                    ['sup_21_93_5_23_3o8b_A_0_781', 
                     'APITAYSQQTRGLLGCIITSLTGRDKNQVDGEVQVLSTATQSFLATC----CWTV--GA-SKTLAGP-GPI-------------W--P-GAR-M----------YLVTRHADVI-------------------YLKGSSGGPLLCP-G----I----------------------/AAVMS']]

        model_filenames = modeller_utils.run_modeller_pepcomposer(aln_data,
                                    cwd=self.tempdir,
                                    atom_files_dirs=os.path.join(self.test_data, 'pepcomposer'),
                                    very_fast=True,
                                    starting_models=1,
                                    ending_models=1) 

        print(model_filenames)
        
    def test_run_modeller_pepcomposer_multi(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        aln_data = [
                    ['9_67_7_523_3o8l_A_0_1771', 
                     'APITAYSQQTRGLLGCIITSLTGRDKNQVDGEVQVLSTATQSFLATCVNGVCWTVYHGAGSKTLAGPKGPITQMYTNVDQDLVGWPAPPGARSMTPCTC/PVSYLKGSSGGPLLCPSGHVVGIFRAAVCTRGVAKAVDFIPVESM/GGIYGYQ'],
                    ['J5UAW_receptor-1', 
                     'APITAYSQQTRGLLGCIITSLTGRDKNQVDGEVQVLSTATQSFLATCVNGVCWTVYHGAGSKTLAGPKGPITQMYTNVDQDLVGWPAPPGARSMTPCTC/PVSYLKGSSGGPLLCPSGHVVGIFRAAVCTRGVAKAVDFIPVESM/-------'], 
                    ['sup_9_67_7_523_3o8l_A_0_1771', 
                     'APITAYSQQTRGLLGCIITSLTGRDKNQVDGEVQVLSTATQSFLATC----CWTV--GA-SKTLAGP-GPI-------------W--P-GAR-M-----/---YLKGSSGGPLLCP-G----I----------------------/GGIYGYQ']]
        model_filenames = modeller_utils.run_modeller_pepcomposer(aln_data,
                                    cwd=self.tempdir,
                                    atom_files_dirs=os.path.join(self.test_data, 'pepcomposer', 'multi'),
                                    very_fast=True,
                                    starting_models=1,
                                    ending_models=1)
        
        print(model_filenames)

    def test_run_modeller_pepcomposer_multi_and_break(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        aln_data = [
                    ['9_67_7_523_3o8l_A_0_1771',
                     'APITAYSQQTRGLLGCIITSLTGRDKNQVDGEVQVLSTATQSFLATCVNGVCWT/PKGPITQMYTNVDQDLVGWPAPPGARSMTPCTC/PVSYLKGSSGGPLLCPSGHVVGIFRAAVCTRGVAKAVDFIPVESM/GGIYGYQ'],
                    ['J5UAW_receptor-2',
                     'APITAYSQQTRGLLGCIITSLTGRDKNQVDGEVQVLSTATQSFLATCVNGVCWT-PKGPITQMYTNVDQDLVGWPAPPGARSMTPCTC/PVSYLKGSSGGPLLCPSGHVVGIFRAAVCTRGVAKAVDFIPVESM/-------'], 
                    ['sup_9_67_7_523_3o8l_A_0_1771-1', 
                     'APITAYSQQTRGLLGCIITSLTGRDKNQVDGEVQVLSTATQSFLATC----CWT-P-GPI-------------W--P-GAR-M-----/---YLKGSSGGPLLCP-G----I----------------------/GGIYGYQ']]
        model_filenames = modeller_utils.run_modeller_pepcomposer(aln_data,
                                    cwd=self.tempdir,
                                    atom_files_dirs=os.path.join(self.test_data, 'pepcomposer', 'multi_and_break'),
                                    very_fast=True,
                                    starting_models=1,
                                    ending_models=1) 

        print(model_filenames)

    def test_run_modeller_pepcomposer_multi_and_break2(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        aln_data = [
                    ['9_67_7_523_3o8l_A_0_1771',
                     'APITAYSQQTRGLLGCIITSLTGRDKNQVDGEVQVLSTATQSFLATCVNGVCWT/PKGPITQMYTNVDQDLVGWPAPPGARSMTPCTC/PVSYLKGSSG/LLCPSGHVVGIFRAAVCT/KAVDFIPVESM/GGIYGYQ'],
                    ['J5UAW_receptor-4',
                     'APITAYSQQTRGLLGCIITSLTGRDKNQVDGEVQVLSTATQSFLATCVNGVCWT-PKGPITQMYTNVDQDLVGWPAPPGARSMTPCTC/PVSYLKGSSG-LLCPSGHVVGIFRAAVCT-KAVDFIPVESM/-------'],
                    ['sup_9_67_7_523_3o8l_A_0_1771-3',
                     'APITAYSQQTRGLLGCIITSLTGRDKNQVDGEVQVLSTATQSFLATC----CWT-P-GPI-------------W--P-GAR-M-----/---YLKGSSG-LLCP-G----I-------------------/GGIYGYQ']
                    ]
        
        
        model_filenames = modeller_utils.run_modeller_pepcomposer(aln_data,
                                    cwd=self.tempdir,
                                    atom_files_dirs=os.path.join(self.test_data, 'pepcomposer', 'multi_and_break2'),
                                    very_fast=True,
                                    starting_models=1,
                                    ending_models=1) 

        print(model_filenames)

        
    def test_from_raw_seqs_to_model_SINGLE_TEMPLATE(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)

        pir, num_map, alnobj = modeller_utils.create_modeller_pir(self.aln_SINGLE_TEMPLATE, chain_config={'2odi_A': 'A'})
        
        temp_pir = tempfile.mkstemp(dir=self.tempdir)[1]
        
        f = open(temp_pir, 'w')
        f.write(pir)
        f.close()

        modeller_utils.run_modeller(alnfile=temp_pir,
                                    knowns=('2odi_A',),
                                    sequence='TARGET',
                                    cwd=self.tempdir,
                                    atom_files_dirs=[self.test_data],
                                    very_fast=True,
                                    starting_models=1,
                                    ending_models=1)

    def test_from_raw_seqs_to_model_MULTI_TEMPLATE(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        
        pir, num_map, alnobj = modeller_utils.create_modeller_pir(self.aln_MULTI_TEMPLATE,
                                                 chain_config={
                                                               '2odi_A': 'A',
                                                               '2aor_A': 'A',
                                                               '1azo_A': 'A',
                                                               }
                                                 )
        temp_pir = tempfile.mkstemp(dir=self.tempdir)[1]
        
        f = open(temp_pir, 'w')
        f.write(pir)
        f.close()
    
        out_model_filenames = modeller_utils.run_modeller(alnfile=temp_pir,
                                    knowns=('2odi_A', '2aor_A', '1azo_A'),
                                    sequence='TARGET',
                                    cwd=self.tempdir,
                                    atom_files_dirs=[self.test_data],
                                    very_fast=True,
                                    starting_models=1,
                                    ending_models=1)
        print(out_model_filenames)

    def test_get_B_factors_resi(self):
        pdbfilename = os.path.join(self.test_data, self.clean_pdbfilename)
        resi_B_factors = modeller_utils.get_B_factors_resi(pdbfilename)
        print(resi_B_factors)
        
#    @unittest.skip('')
    def test_extract_modeller_atom_seq(self):
        pdbfilename = os.path.join(self.test_data, self.clean_pdbfilename)

        seq = modeller_utils.extract_modeller_atom_seq(pdbfilename)
        self.assertEqual(seq, 'MIEIKDKQLTGLRFIDLFAGLGGFRLALESCGAECVYSNEWDKYAQEVYEMNFGEKPEGDI\
TQVNEKTIPDHDILCAGFPCQAFSISGKQKGFEDSRGTLFFDIARIVREKKPKVVFMENVKNFASHDNGNTLEVVKNTMNELDYSFHAKVLN\
ALDYGIPQKRERIYMICFRNDLNIQNFQFPKPFELNTFVKDLLLPDSEVEHLVIDRKDLVMTNQEIEQTTPKTVRLGIVGKGGQGERIYSTR\
GIAITLSAYGGGIFAKTGGYLVNGKTRKLHPRECARVMGYPDSYKVHPSTSQAYKQFGNSVVINVLQYIAYNIGSSLNFKPY')
 
    def test_extract_modeller_atom_seq_with_numbers(self):
        pdbfilename = os.path.join(self.test_data, self.clean_pdbfilename)
        result = modeller_utils.extract_modeller_atom_seq_with_numbers(pdbfilename)
        print(result)

    def test_extract_modeller_atom_seq_with_numbers_2OA9(self):
        """2OA9 has negative residue numbers"""
        pdbfilename = os.path.join(self.test_data, '2OA9.pdb')
        result = modeller_utils.extract_modeller_atom_seq_with_numbers(pdbfilename)
        print(result)
        print(len(result[0]), len(result[1]))
        
#    @unittest.skip('')
    def test_run_modeller_from_fasta(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        modeller_utils.run_modeller_from_fasta(alnfilename=os.path.join(self.test_data, 'aln.fasta'),
                                               knowns=('1REW', '2PJY'),
                                               sequence='GDF_ALK',
                                               atom_files_dirs=[self.test_data],
                                               cwd=self.tempdir,
                                               very_fast=True,
                                               starting_models=1,
                                               ending_models=1)

    def test_run_modeller_from_fasta_BcnI_on2oaa_A(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        print(modeller_utils.extract_modeller_atom_seq(os.path.join(self.test_data, '2oaa_A.pdb')))
        modeller_utils.run_modeller_from_fasta(alnfilename=os.path.join(self.test_data, 'BcnI.fasta'),
                                               knowns=('2oaa_A',),
                                               sequence='BcnI_new',
                                               atom_files_dirs=[self.test_data],
                                               cwd=self.tempdir,
                                               very_fast=True,
                                               starting_models=1,
                                               ending_models=1)

    def test_run_modeller_from_fasta_BcnI_on2oaa_A_no_tails_no_inserts(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
#        print(modeller_utils.extract_modeller_atom_seq(os.path.join(self.test_data, '2oaa_A.pdb')))
        modeller_utils.run_modeller_from_fasta(alnfilename=os.path.join(self.test_data, 'BcnI.fasta'),
                                               knowns=('2oaa_A',),
                                               sequence='BcnI_new',
                                               atom_files_dirs=[self.test_data],
                                               cwd=self.tempdir,
                                               very_fast=True,
                                               starting_models=1,
                                               ending_models=1,
                                               skip_tails=True,
                                               skip_inserts=True)

    def test_run_modeller_multichain(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        modeller_utils.run_modeller_from_fasta(alnfilename=os.path.join(self.test_data, 'Nup53.aln.fasta'),
                                               knowns=('4LIR.clean',),
                                               sequence='Nup53',
                                               atom_files_dirs=[self.test_data],
                                               cwd=self.tempdir,
                                               very_fast=True,
                                               starting_models=1,
                                               ending_models=1,
                                               skip_tails=True,
                                               flanks=1,
                                               with_breaks=True)

        model_seq, model_resi_nums = modeller_utils.extract_modeller_atom_seq_with_numbers(os.path.join(self.tempdir, 'Nup53.B99990001.pdb'))
        self.assertEqual(model_seq, 'LDDSWVTVFGFPQASASYILLQFAQYGNILKHVMSNTGNWMHIRYQSKLQARKALSKDGRIFGESIMIGVKPCIDKSVME/DHLDDSWVTVFGFPQASASYILLQFAQYGNILKHVMSNTGNWMHIRYQSKLQARKALSKDGRIFGESIMIGVKPCIDKSVMESS')
        self.assertEqual(model_resi_nums, ['171', '172', '173', '174', '175', '176', '177', '178', '179', '180', '181', '182', '183', '184', '185', '186', '187', '188', '189', '190', '191', '192', '193', '194', '195', '196', '197', '198', '199', '200', '201', '202', '203', '204', '205', '206', '207', '208', '209', '210', '211', '212', '213', '214', '215', '216', '217', '218', '219', '220', '221', '222', '223', '224', '225', '226', '227', '228', '229', '230', '231', '232', '233', '234', '235', '236', '237', '238', '239', '240', '241', '242', '243', '244', '245', '246', '247', '248', '249', '250', '169', '170', '171', '172', '173', '174', '175', '176', '177', '178', '179', '180', '181', '182', '183', '184', '185', '186', '187', '188', '189', '190', '191', '192', '193', '194', '195', '196', '197', '198', '199', '200', '201', '202', '203', '204', '205', '206', '207', '208', '209', '210', '211', '212', '213', '214', '215', '216', '217', '218', '219', '220', '221', '222', '223', '224', '225', '226', '227', '228', '229', '230', '231', '232', '233', '234', '235', '236', '237', '238', '239', '240', '241', '242', '243', '244', '245', '246', '247', '248', '249', '250', '251', '252'])

    def test_run_modeller_multichain_2(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        modeller_utils.run_modeller_from_fasta(alnfilename=os.path.join(self.test_data, 'Nsp1_Nup146_Nup82_5CWS_aln1.fasta'),
                                               knowns=('5CWS_hybrid_helix_template1',),
                                               sequence='Nsp1_Nup146_Nup82',
                                               atom_files_dirs=[self.test_data],
                                               cwd=self.tempdir,
                                               very_fast=True,
                                               starting_models=1,
                                               ending_models=1,
                                               skip_tails=True,
                                               skip_inserts=True,
                                               flanks=2,
                                               with_breaks=True)

        model_seq, model_resi_nums = modeller_utils.extract_modeller_atom_seq_with_numbers(os.path.join(self.tempdir, 'Nsp1_Nup146_Nup82.B99990001.pdb'))
        print(model_seq)
        print(model_resi_nums)
        self.assertEqual(model_seq, 'LQEILNKWSTDLTTQTEVFNKLCDQVSDWDRTLVDNGALISKLYTETVEAEQMSNRIDDGLEYVSSSQQELFKLLDSYETQLETFDGRATSERAFGVADDILSRLDRLGEDLGTVINQMNDFSKPDDSISEIVKVLNAQLASLGWVENRIFQMEEKLDTIKK/VNTFERLYLQFKNELDLVWQNINIIAEYIQATMAMLSQNTEELEDLLDTVTSFSAFCSDYSKQIDYLEACLVRINAKRIQVTRLLKALTNELRQLGPEALRRQKELRLKMEKVLKSLSTLEQQAVDARMSDTRRFKKPTLGSIEVAYSRIATLLGQRLKQLYKLEKDIKRMATKKKM/SASLRFLGKVVARYRETLNLLDHGCSELHHRLKLQREEYERQQNHIYKLSDRISNFREKAWSTEHLEHLTSDMSMCEKRIDQVLQRVMDLRVPDLSDKEKQFIKEIGNYKEKVTGEGIEKRVETLKTLLQRTKPRDAQTTLVASSSDMRLAAIEQLQKLLAQQSLSIKELKTKTVSFQRLLQTS')
        self.assertEqual(model_resi_nums, ['424', '425', '426', '427', '428', '429', '430', '431', '432', '433', '434', '435', '436', '437', '438', '439', '440', '441', '442', '443', '444', '445', '446', '447', '448', '449', '450', '451', '452', '453', '454', '455', '456', '457', '458', '459', '460', '461', '462', '463', '464', '465', '466', '467', '468', '469', '470', '471', '472', '473', '474', '475', '476', '477', '478', '479', '480', '481', '482', '483', '484', '485', '486', '487', '488', '489', '490', '491', '492', '493', '494', '495', '496', '497', '498', '499', '500', '501', '502', '503', '504', '505', '506', '507', '508', '509', '510', '511', '512', '513', '514', '521', '522', '523', '524', '525', '526', '527', '528', '529', '530', '531', '532', '533', '534', '535', '536', '537', '538', '539', '540', '541', '542', '543', '544', '545', '546', '547', '548', '549', '550', '551', '552', '553', '554', '555', '556', '557', '558', '559', '560', '561', '562', '563', '564', '565', '566', '567', '568', '569', '570', '571', '572', '573', '574', '575', '576', '577', '578', '579', '580', '581', '582', '583', '584', '585', '586', '587', '588', '589', '590', '591', '1074', '1075', '1076', '1077', '1078', '1079', '1080', '1081', '1082', '1083', '1084', '1085', '1086', '1087', '1088', '1089', '1090', '1091', '1092', '1093', '1094', '1095', '1096', '1097', '1098', '1099', '1100', '1101', '1102', '1115', '1116', '1117', '1118', '1119', '1120', '1121', '1122', '1123', '1124', '1125', '1126', '1127', '1128', '1129', '1130', '1131', '1132', '1133', '1134', '1135', '1136', '1137', '1138', '1139', '1140', '1141', '1142', '1143', '1144', '1145', '1146', '1147', '1148', '1149', '1150', '1151', '1152', '1153', '1154', '1155', '1156', '1157', '1158', '1159', '1160', '1161', '1162', '1163', '1164', '1165', '1166', '1167', '1168', '1169', '1170', '1171', '1172', '1173', '1174', '1175', '1183', '1184', '1185', '1186', '1187', '1188', '1189', '1190', '1191', '1192', '1193', '1194', '1195', '1196', '1197', '1198', '1199', '1200', '1201', '1202', '1203', '1204', '1205', '1206', '1207', '1208', '1209', '1210', '1211', '1212', '1213', '1214', '1215', '1216', '1217', '1218', '1219', '1220', '1221', '1222', '1223', '1224', '1225', '1226', '1227', '1228', '1229', '1230', '1231', '1232', '1233', '1234', '1235', '1236', '1237', '1238', '1239', '1240', '1241', '1242', '1243', '1244', '1245', '1246', '1247', '1248', '1249', '1250', '1251', '1252', '1253', '1254', '1255', '1256', '1257', '1258', '1259', '1260', '1261', '1262', '1263', '1264', '1265', '1266', '1267', '1268', '1269', '619', '620', '621', '622', '623', '624', '625', '626', '627', '628', '629', '630', '631', '632', '633', '634', '635', '636', '637', '638', '639', '640', '641', '642', '643', '644', '645', '646', '647', '648', '649', '650', '651', '652', '653', '654', '655', '656', '657', '658', '659', '660', '661', '662', '663', '664', '665', '666', '667', '668', '669', '670', '671', '672', '673', '674', '675', '676', '677', '678', '679', '680', '681', '682', '683', '684', '685', '686', '687', '688', '689', '690', '691', '692', '693', '694', '695', '696', '697', '698', '699', '700', '701', '702', '703', '704', '705', '706', '707', '708', '709', '710', '711', '712', '713', '714', '715', '716', '717', '718', '719', '720', '721', '722', '723', '724', '725', '726', '727', '728', '729', '730', '731', '732', '733', '734', '736', '737', '738', '739', '740', '741', '742', '743', '744', '745', '746', '747', '748', '749', '750', '751', '752', '753', '754', '755', '756', '757', '758', '759', '760', '761', '762', '763', '764', '765', '766', '767', '768', '769', '770', '771', '772', '773', '774', '775', '776', '777', '778', '779', '780', '781', '782', '783', '784', '785', '786', '787', '788', '789', '790', '791', '792', '793', '794', '795', '796', '797', '798', '799', '800', '801', '802', '803'])


    def test_rotate_translate_pdb(self):
        """Should rotate 1G55 to be superposed with 3MHT"""
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir        )
        pdbfilename = os.path.join(self.test_data, '1G55.pdb')
        out_pdbfilename = os.path.join(self.tempdir, '1G55.out.pdb')

        translate = [-6.3424,  -27.9530,  137.2810]

        rotate = [[0.4573386, -0.3554735, -0.8151564],
                  [0.8036969, -0.2271638,  0.5499709],
                  [-0.3806741, -0.9066616,  0.1818021]]

        modeller_utils.rotate_translate_pdb(pdbfilename, out_pdbfilename, translate, rotate)
        
    def test_translate_rotate_pdb_angle_axis(self):
        """Should rotate 1DCT to be superposed with 1G55"""
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        

        translate = [18.1668,   72.8398,  -39.5607]
        translate = [-a for a in translate]
        rotate = [[0.6086824, -0.3459784, -0.7140061],
                  [-0.3756564,  0.6669862, -0.6434374],
                  [0.6988477,  0.6598700,  0.2760137]]
        rotate = None
        angle = 1.2913317
        angle = -angle
        import math
        angle = math.degrees(angle)
        axis = [0.6779562, -0.7349402, -0.0154379]
        angle_axis = [angle, axis]
        pdbfilename = os.path.join(self.test_data, '1DCT.pdb')
        out_pdbfilename = os.path.join(self.tempdir, '1DCT.out.pdb')
                
        modeller_utils.translate_rotate_pdb(pdbfilename, out_pdbfilename, translate, rotate, angle_axis)

    def test_refine_aln_fit(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        aln = [
               ['1x9z_A', '-----------------------------------------------------------------------------------------------QSFG---RVLTIVHSDCALLERDGNISLLSLPVAERWLRQAQLTPGEAPVCAQPLLIPLRLKVSAEEKSALEKAQSALAELGIDF---QSDA--QHVTIRAVPLPLRQQNLQILIPELIGYLAKQSVF------EPGNIAQWIARNLMSEHAQWSMAQAITLLADVERL-----CPQLVKTPPGGLLQSVDLHPAIKALKD--------------------------------------------------------------------------------------------------'],
               ['1fx7_A', 'MNELVDTTEMYLRTIYDLEEEGVTPLRARIAERLDQSGPTVSQTVSRMERDGLLRVAGDRHLELTEKGRALAIAVMRKHRLAERLLVDVIGLPWE---------------------------------------------------------------------------------------------------------------------------------------------EVHAEACRWEHVMSEDVERRLVKVL---NNPTTSPFGNPI--------------------PGLDELGVGPEPGADDANLVRLTELPAGSPVAVVVRQLTEHVQGDIDLITRLKDAGVVPNARVTVETTPGGGVTIVIPGHENVTLPHEMAHAVKVEKV']
               ]

        pir = modeller_utils.create_modeller_pir_structures(aln, chain_config={'1x9z_A': 'A', '1fx7_A': 'A'})
        
        temp_pir = tempfile.mkstemp(dir=self.tempdir)[1]
        
        f = open(temp_pir, 'w')
        f.write(pir)
        f.close()
        
        modeller_utils.refine_aln_fit(alnfile=temp_pir,
                                    knowns=('1x9z_A', '1fx7_A'),
                                    cwd=self.tempdir,
                                    atom_files_dirs=[self.test_data],
                                    )


    def test_extend_aln_fit(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        aln = [
               ['_fix_pos', '0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000044444444444444444444444440000000044444440000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'],
               ['1x9z_A',   '-----------------------------------------------------------------------------------------------QSFG---RVLTIVHSDCALLERDGNISLLSLPVAERWLRQAQLTPGEAPVCAQPLLIPLRLKVSAEEKSALEKAQSALAELGIDF---QSDA--QHVTIRAVPLPLRQQNLQILIPELIGYLAKQSVF------EPGNIAQWIARNLMSEHAQWSMAQAITLLADVERL-----CPQLVKTPPGGLLQSVDLHPAIKALKD--------------------------------------------------------------------------------------------------'],
               ['1fx7_A_fit',   'MNELVDTTEMYLRTIYDLEEEGVTPLRARIAERLDQSGPTVSQTVSRMERDGLLRVAGDRHLELTEKGRALAIAVMRKHRLAERLLVDVIGLPWE---------------------------------------------------------------------------------------------------------------------------------------------EVHAEACRWEHVMSEDVERRLVKVL---NNPTTSPFGNPI--------------------PGLDELGVGPEPGADDANLVRLTELPAGSPVAVVVRQLTEHVQGDIDLITRLKDAGVVPNARVTVETTPGGGVTIVIPGHENVTLPHEMAHAVKVEKV']
               
               ]


        pir, num_map, alnobj = modeller_utils.create_modeller_pir(aln,
                                                 chain_config={'1x9z_A': 'A', '1fx7_A_fit': 'A'}
                                                 )
        
        temp_pir = tempfile.mkstemp(dir=self.tempdir)[1]
        
        f = open(temp_pir, 'w')
        f.write(pir)
        f.close()
        
        modeller_utils.extend_aln_fit(alnfile=temp_pir,
                                    knowns=('1x9z_A', '1fx7_A_fit', '_fix_pos'),
                                    cwd=self.tempdir,
                                    atom_files_dirs=[self.test_data],
                                    )


    def test_fit_from_seq_aln(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
#        aln = [
#               ['1x9z_A',   '-----------------------------------------------------------------------------------------------QSFG---RVLTIVHSDCALLERDGNISLLSLPVAERWLRQAQLTPGEAPVCAQPLLIPLRLKVSAEEKSALEKAQSALAELGIDF---QSDA--QHVTIRAVPLPLRQQNLQILIPELIGYLAKQSVF------EPGNIAQWIARNLMSEHAQWSMAQAITLLADVERL-----CPQLVKTPPGGLLQSVDLHPAIKALKD--------------------------------------------------------------------------------------------------'],
#               ['1fx7_A',   'MNELVDTTEMYLRTIYDLEEEGVTPLRARIAERLDQSGPTVSQTVSRMERDGLLRVAGDRHLELTEKGRALAIAVMRKHRLAERLLVDVIGLPWE---------------------------------------------------------------------------------------------------------------------------------------------EVHAEACRWEHVMSEDVERRLVKVL---NNPTTSPFGNPI--------------------PGLDELGVGPEPGADDANLVRLTELPAGSPVAVVVRQLTEHVQGDIDLITRLKDAGVVPNARVTVETTPGGGVTIVIPGHENVTLPHEMAHAVKVEKV']
#               ]

        aln = [
            ['1x9z_A',   'QSFG------------------------------------------------------------------------------------------------------RVLTIVHSDCALLERDGNISLLSLPVAERWLRQAQLTPGEAPVCAQPLLIPLRLKVSAEEKSALEKAQSALAELGIDF---QSDA--QHVTIRAVPLPLRQQNLQILIPELIGYLAKQSVF------EPGNIAQWIARNLMSEHAQWSMAQAITLLADVERL-----CPQLVKT-------------PPGGLLQSVDLHPAIKALKD--------------------------------------------------------------------------------------------------'],
            ['1fx7_A',   '----MNELVDTTEMYLRTIYDLEEEGVTPLRARIAERLDQSGPTVSQTVSRMERDGLLRVAGDRHLELTEKGRALAIAVMRKHRLAERLLVDVIGLPWE---------------------------------------------------------------------------------------------------------------------------------------------EVHAEACRWEHVMSEDVERRLVKVL---NNPTTSPFGNPI---------------------------------PGLDELGVGPEPGADDANLVRLTELPAGSPVAVVVRQLTEHVQGDIDLITRLKDAGVVPNARVTVETTPGGGVTIVIPGHENVTLPHEMAHAVKVEKV']
               
               ]
        pir = modeller_utils.create_modeller_pir_structures(aln,
                                                 chain_config={'1x9z_A': 'A', '1fx7_A': 'A'}
                                                 )
        
        temp_pir = tempfile.mkstemp(dir=self.tempdir)[1]
        
        f = open(temp_pir, 'w')
        f.write(pir)
        f.close()
        
        modeller_utils.fit_from_seq_aln(alnfile=temp_pir,
#                                    knowns=('1x9z_A', '1fx7_A',),
                                    cwd=self.tempdir,
                                    atom_files_dirs=[self.test_data],
                                    )

    @unittest.skip('')
    def test_gen_struct_aln(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        modeller_utils.gen_struct_aln(
                                    knowns=('1x9z_A', '1fx7_A_fit'),
                                    cwd=self.tempdir,
                                    atom_files_dirs=[self.test_data],
                                    )        

#    def test_getting_and_restoring_log_level(self):
#        log.verbose() #log is now verbose
#        old = modeller_utils._get_log_level()
#        self.assertEqual(old, (1, 1, 1, 1, 1))
#        log.none()    #log is now none
#        temp = modeller_utils._get_log_level()
#        self.assertEqual(temp, (0, 0, 0, 0, 0))
#        log.level(*old) #log is verbose again
#        restored = modeller_utils._get_log_level()
#        self.assertEqual(restored, (1, 1, 1, 1, 1))

    def test_ligands(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        pir_filename = os.path.join(self.test_data, 'HHSEARCH.BcnI.2oaa_A.pir')
        modeller_utils.run_modeller(alnfile=pir_filename,
                                    knowns=('2oaa_A_155',),
                                    sequence='TARGET',
                                    cwd=self.tempdir,
                                    atom_files_dirs=[self.test_data],
                                    very_fast=True,
                                    starting_models=1,
                                    ending_models=1,
                                    with_breaks=True,
                                    ligands=True)        


    def test_create_modeller_pir_LIGANDS(self):
        aln = [['TARGET',
                '/-----KEEVVNKLHEIKNKGYLPTFRTDDGVVGQILERQFGVQENNITLGDLGEFELKGMRRKAKSNLTLFHKKPV-AGQTVIQIFNRFGYVKPSMKKKLFTTIKGGRLNN-----LGLTLNA-KHASEINLYYQD------EYLSTWDLN------LSKIEKLVLVFAETIGRSPEEQFHFTKAYMLTEIN--DITSLINDGVLVMDLCIDQDLSKS-K-GPHDRGPHLRIPISKLDLYRNIERL-'],
               [u'2oaa_A_155',
                '-SMSEYLNLLKEAIQNVVDGGWHETKRKGNTGIGKTFEDLLEKEEDNLDAPDFHDIEIKTHETAAKSLLTLFTKSPTNPRGANTMLRNRYGKKDEYGNNILHQTVSGNRKTNSNSYNYDFKIDIDWESQVVRLEVFDKQDIMIDNSVYWSFDSLQNQLDKKLKYIAVISAESKIENEKKYYKYNSANLFTDLTVQSLCRGIENGDIKVDIRIGAYHSGKKKGKTHDHGTAFRINMEKLLEYGEVKVIV']]
#        pdb_filename = os.path.join(self.test_data, '2oaa_A_155.pdb')
        
        pdb_filename = os.path.join(self.test_data, '2OAA.pdb')
        pir, num_map, alnobj = modeller_utils.create_modeller_pir(aln,
                                                 chain_config={'2oaa_A_155': 'A'},
                                                 ligands=True,
                                                 ligands_cfg=[
                                                              ['C', []],
                                                              ['D', []]
                                                              ],
                                                 pdbfilenames={'2oaa_A_155': pdb_filename})
if __name__ == '__main__':
    unittest.main()
