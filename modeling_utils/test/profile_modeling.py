import os
import tempfile

from PyPlatform.modeling import modeller_utils

test_data = os.path.join(os.getcwd(), 'test_modeller_utils_data')
        
aln_MULTI_TEMPLATE = [
        ['TARGET', '--------------MKIWSKEEVVNK--------LHEIKNKGYLSVPTDMFRTDDGVVGQ\
ILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYV\
KPSSRNPEVMKKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIE\
KLVLVFAETIGRANSPEEQFHFTKAYMLT----EI-------NDITSLINDGVLVMDLCI\
DQDLSKSKGPHDRGPHLRIPISKLDKLYRNIERLL-------------------------\
------------------------------------------------------------\
------------------------------------------------------------\
----'],
        ['2odi_A', '--------------MKIWSKEEVVNK--------LHEIKNKGYLSVPTDMFRTDDGVVGQ\
ILERQFGVQENNITLGDL--GEFELKGMRNRKAKSNLTLFHKKPVAGQTVIQIFNRFGYV\
KPSSRNPEVMKKKLFTTIKGGRLNNLGLTLNAKHASEINLYYQDEYLSTWDLN--LSKIE\
KLVLVFAETIGRANSPEEQFHFTKAYMLT----EI-------NDITSLINDGVLVMDLCI\
DQDLSKSKGPHDRGPHLRIPISKLDKLYRNIERLL-------------------------\
------------------------------------------------------------\
------------------------------------------------------------\
----'],
        ['2aor_A', '------------MI--PQTLEQLLSQAQSIAGLTFGELADELHIPVPIDL-KRDKGWVGM\
LLERALGATAGSKAEQDFSHLGVELKTLP-------------------------------\
------------------------------------------------------------\
------------------------------------------------------------\
-----------------------------------INAEGYPLETTFVSLAPLVQNSGVK\
WENSHVRHKLSCVLWMPIEGSRHIPLRERHIGAPIFWKPTAEQERQLKQDWEELMDLIVL\
GKLDQITARIGEVMQLRPKGANSRAVTKGIGKNGEIIDTLPLGFYLRKEFTAQILNAFLE\
T---'],
        ['1azo_A', '------PRPLLSPP---ETEEQLLAQAQQLSGYTLGELAALVGLVTPENL-KRDKGWIGV\
LLEIWLGA-----PEQDFAALGVELKTIP-VDSLGR------------------------\
------PLE--TTFVCVA------------P----------LTGNSGVTWETSHVRHKLK\
RVLWIPVEGE--ASIPLAQRRVGSPLLWSPNEEEDRQLREDWEELMDMIVLGQV------\
-----------------------------------ERITARHGEYLQIRP-------LTE\
AIGARGERILTLPRGFYLKKNFTSALLARHFLIQ--------------------------\
------------------------------------------------------------\
----']
        ]

tempdir = tempfile.mkdtemp()
print tempdir

pir = modeller_utils.create_modeller_pir(aln_MULTI_TEMPLATE,
                                         chain_config={
                                                       '2odi_A': 'A',
                                                       '2aor_A': 'A',
                                                       '1azo_A': 'A',
                                                       }
                                         )
temp_pir = tempfile.mkstemp(dir=tempdir)[1]

f = open(temp_pir, 'w')
f.write(pir)
f.close()

modeller_utils.run_modeller(alnfile=temp_pir,
                            knowns=('2odi_A', '2aor_A', '1azo_A'),
                            sequence='TARGET',
                            cwd=tempdir,
                            atom_files_dirs=[test_data],
                            very_fast=True,
                            starting_models=1,
                            ending_models=1)    