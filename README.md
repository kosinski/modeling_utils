# About

modeling_utils provide a modeling script that give a sequence alignment automates the most common tasks in homology modeling and provides utilities to prepare the input.

It is quite a ground-braking script because it features:
* oligomeric/multi-chain modeling and multiple templates
* optional removal of tails and inserts 
* and keeping the correct residue numbering with the above
* setting unique chain ids to the output chains
* provides command line options for controlling optimization levels and number of output models

# Installation
Use it as a normal Python package
## From source

### Obtain a copy from Kosinski Lab

### For authorized EMBL users:
```
git clone git@git.embl.de:kosinski/modeling_utils.git
```
and then for example by setting PYTHONPATH:
```
export PYTHONPATH=/Users/kosinski/devel/modeling_utils/$PYTHONPATH
```

## Using pip
```
pip install ... will do later
```
## Install dependencies

### Python 2.7
You figure out.
### Modeller
You figure out.

# Usage

## 1. Prepare sequence alignment
* The modeling script takes a FASTA-formatted file with the sequence alignment.

    ```
    >target_name
    sequence
    >template1
    sequence
    >template2
    sequence
    ```
   * The 1st sequence is the target sequence to be modelled. The name does not matter. Use the FULL sequence to preserve the residue numbering, do not cut anything, the script will take care of this.
   * Template names have to correspond to the template PDB filenames, as in Modeller. For example, if you template has a name my_template.pdb - the name should be "my_template". Avoid usign weird characters like dots, spaces etc. Use the sequences exactly as in the PDB files (most servers give you such sequences in the output, otherwise use `extract_modeller_atom_seq.py` script)
* Multi-chain/oligomeric modeling modeling
   * For multi-chain/oligomeric modeling, chains needs to be seperated by "/" character and be in order as in the PDB file. For example:
        ```
        >GDF_ALK
        NLCHRHQLFINFRDLGWHKWIIAPKGFMANYCHGECPFSLTISLNSSNYA
        FMQALMHAVDPEIPQAVCIPTKLSPISMLYQDNNDNVILRHYEDMVVDEC
        GCG/-NLCHRHQLFINFRDLGWHKWIIAPKGFMANYCHGECPFSLTISLNS
        SNYAFMQALMHAVDPEIPQAVCIPTKLSPISMLYQDNNDNVILRHYEDMV
        VDECGCG/--SPGLKCVCLLC---DSSNFTCQTEGACWASVMLTN-GKEQV
        IKSCVSLPEL---NAQVFCHS---SNN---VTKTECCFTDFCNNITLHLP
        TA-
        >1REW
        SSCKRHPLYVDFSDVGWNDWIVAPPGYHAFYCHGECPFPLADHLNSTNHA
        IVQTLVNSVNSKIPKACCVPTELSAISMLYLDENEKVVLKNYQDMVVEGC
        GCR/-SSCKRHPLYVDFSDVGWNDWIVAPPGYHAFYCHGECPFPLADHLNS
        TNHAIVQTLVNSVNSKIPKACCVPTELSAISMLYLDENEKVVLKNYQDMV
        VEGCGCR/-TLPFLKCYCSGHCPDDAINNTCITNGHCFAIIEEDDQGETTL
        ASGCMKY-EG----SDFQCKD---SPKAQLRRTIECCRTNLCNQYLQPTL
        PP-
        >2PJY
        --------------------------------------------------
        --------------------------------------------------
        ---/-----------------------------------------------
        --------------------------------------------------
        -------/----ALQCFCHLC---TKDNFTCVTDGLCFVSVTETT-DKVIH
        NSSCIAEIDLIPRDRPFVCAPSSKTGS---VTTTYCCNQDHCNKIEL---
        ---
        ```
        so "/" in **every** sequence and at the same position
  * We provide `scripts/merge_alns.py` to help preapring the multi-chain alignment from alignments of individual chains:
    1. Prepare FASTA alignments for each target-template chain separately.
    1. Merge:
        ```
        Usage: python merge_alns.py [options] out_seq_name1,out_seq_name2 <aln1.fasta> <aln2.fasta> ... > out_aln.fasta"
        ```
        where 
        * out_seq_name1, out_seq_name2 - desired names for sequences in the output file
        * <aln1.fasta> <aln2.fasta> ... - sequence alignments for each chain
## 2. Run:
* Usage:
    ```
    $ python scripts/build_quick_model.py -h
    Usage: build_quick_model.py [options] aln_fasta_filename out_pdb_model_name <atom_files_dirs>

    Options:
    -h, --help            show this help message and exit
    --skip_tails          skip_tails [default: False]
    --skip_inserts        skip_inserts [default: False]
    --skipNter            skipNter [default: False]
    --skipCter            skipCter [default: False]
    --include=INCLUDE_REGIONS
                            File with sequence and template regions to include in
                            modeling
    --optimize            optimize, do not use very_fast option [default: False]
    --max_ca_ca_distance=MAX_CA_CA_DISTANCE
                            max_ca_ca_distance [default: 14]
    --flanks=FLANKS       flanks [default: 0]
    --models=ENDING_MODEL
                            number of output models [default: 1]
    ```
* Example - just build with default options, with [very_fast](https://salilab.org/modeller/manual/node20.html) optimization:
    ```
    export PYTHONPATH=/Users/kosinski/devel/modeling_utils/$PYTHONPATH
    python ~/devel/modeling_utils/scripts/build_quick_model.py aln.fasta out_model_name.pdb
    ```
* Example - skip tails and inserts, with [very_fast](https://salilab.org/modeller/manual/node20.html) optimization:
    ```
    export PYTHONPATH=/Users/kosinski/devel/modeling_utils/$PYTHONPATH
    python ~/devel/modeling_utils/scripts/build_quick_model.py --skip_tails --skip_inserts aln.fasta out_model_name.pdb
    ```
* Example - skip only tails and turn on the Modeller's default optimization:
    ```
    export PYTHONPATH=/Users/kosinski/devel/modeling_utils/$PYTHONPATH
    python ~/devel/modeling_utils/scripts/build_quick_model.py --skip_tails --optimize aln.fasta out_model_name.pdb
    ```
* Example - skip tails and inserts, but leave a bit of two flanking residues around each insert/tail (i.e. 4 residues for inserts and 2 for tails):
    ```
    export PYTHONPATH=/Users/kosinski/devel/modeling_utils/$PYTHONPATH
    python ~/devel/modeling_utils/scripts/build_quick_model.py --skip_tails --skip_inserts --flanks 2 aln.fasta out_model_name.pdb
    ```
## Utility scripts in scripts/ directory:
Hopefully the usage can be checked with `-h` option
* `hhpred_to_full_fasta.py` - to convert the pir alignment from HHpred to fasta and complete the sequences
    ```
    python hhpred_to_full_fasta.py aln.pir full_seqs.fasta > aln.fasta
    ```
    * aln.pir - alignment as from hhpred
    * full_seqs.fasta - full sequences of the proteins in the aln.pir
* `extract_modeller_atom_seq.py` - extract the sequence from the template as seen by Modeller
* `merge_alns.py` - to merge single chain alignments into one multi-chain, see usage as above
## Tricks:

* For elongated structures like TPR/HEAT repeats or coiled-coils use the amazing `--max_ca_ca_distance` option of build_quick_model.py (Explained here: https://salilab.org/modeller/manual/node59.html)
* Do not forget about our ground-breaking pdb_utils library to rename chains, cut PDBs etc. https://git.embl.de/kosinski/pdb_utils

## Examples pipeline from real life:

### Sth from SpNPC
  
```
export PYTHONPATH=/Users/kosinski/devel/modeling_utils/$PYTHONPATH
python ~/devel/modeling_utils/scripts/build_quick_model.py --skip_tails --skip_inserts SpNup107_Nup131_on3i4r_3kfo_refined_aln_w_breaks.fasta SpNup107_Nup131_on3i4r_3kfo.model.temp.pdb
python ~/devel/pdb_utils/scripts/rename_chains.py SpNup107_Nup131_on3i4r_3kfo.model.temp.pdb A:L B:K > SpNup107_Nup131_on3i4r_3kfo.model.pdb
rm SpNup107_Nup131_on3i4r_3kfo.model.temp.pdb
```
### Multi-chain modeling and some post-processing

```
python merge_alns.py SpY,4XMM SpSec13_swiss_pdb_viewer_on4xmmA.fasta SpNup189C_hhpred_on4xmmB.fasta SpSeh1_hhpred_on4xmmC.fasta SpNup85_hhpred_on4xmmD.fasta SpNup120_hhpred_on4xmmE.fasta SpNup107_hhpred_on4xmmF.fasta > SpY_on4xmm_hhpred.fasta
python ~/devel/modeling_utils/scripts/build_quick_model.py --skip_tails --skip_inserts SpY_on4xmm_hhpred.fasta SpY_on4xmm.model.temp.pdb
python ~/devel/pdb_utils/scripts/rename_chains.py SpY_on4xmm.model.temp.pdb A:N B:M C:O D:P E:R F:L > SpY_on4xmm.model.temp1.pdb
python ~/devel/pdb_utils/scripts/cut_pdb.py SpY_on4xmm.model.temp1.pdb N: M: O: P: R:953-10000 L: > SpY_on4xmm.model.pdb
rm SpY_on4xmm.model.temp.pdb SpY_on4xmm.model.temp1.pdb
```

