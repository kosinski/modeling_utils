from optparse import OptionParser

# from modeling_utils import modeller_utils

from modeller import *
from modeller.scripts import complete_pdb

def main():
    usage = "usage: %prog [options] pdb_file out_pdb_file"
    parser = OptionParser(usage=usage)


    (options, args) = parser.parse_args()

    pdb_filename = args[0]
    out_pdb_file = args[1]
    env = environ()

    env.io.atom_files_directory = ['./']
    env.edat.dynamic_sphere = True

    env.libs.topology.read(file='$(LIB)/top_heav.lib')
    env.libs.parameters.read(file='$(LIB)/par.lib')

    code = pdb_filename
    mdl = complete_pdb(env, code, transfer_res_num=True)

    mdl.write(file=out_pdb_file)

if __name__ == '__main__':
    main()

