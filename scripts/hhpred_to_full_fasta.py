import sys
from itertools import groupby

def fasta_iter(fasta_name):
    """
    given a fasta file. yield tuples of header, sequence
    """
    fh = open(fasta_name)
    # ditch the boolean (x[0]) and just keep the header or sequence since
    # we know they alternate.
    faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
    for header in faiter:
        # drop the ">"
        header = next(header)[1:].strip()
        # join all sequence lines to one.
        seq = "".join(s.strip() for s in next(faiter))
        seq = seq
        yield header, seq

def pir_iter(pir_name):
    """
    given a fasta file. yield tuples of header, sequence
    """
    fh = open(pir_name)
    # ditch the boolean (x[0]) and just keep the header or sequence since
    # we know they alternate.
    faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
    for header in faiter:
        # drop the ">"
        header = next(header)[1:].strip()
        # join all sequence lines to one.
        seq_lines = list(next(faiter))
        header = header + ';' + seq_lines[0].strip() # put pir annotations to header
        seq = "|".join([s.strip() for s in seq_lines[1:]]) 
        seq = seq.strip('*')
        yield header, seq

pir_aln_f = sys.argv[1]
full_seqs_fn = sys.argv[2]

full_seqs = list(fasta_iter(full_seqs_fn))
# nfames = []
# full_seqs = []
# for name, seq in full_seqs:
#     names.append(name)
#     full_seqs.append(seq)

aln = list(pir_iter(pir_aln_f))

out_names = [full_seqs[0][0]]
for name, seq in aln[1:]:
    out_names.append(name)

n_tails = []
c_tails = []

row = 0
for full_seq, aln_seq in zip(full_seqs, aln):
    full_name, full_seq = full_seq
    aln_name, aln_seq = aln_seq
    aln_seq_degapped = aln_seq.replace('-','')
    items = full_seq.replace(aln_seq_degapped,'|').split('|')
    if items:
        n_tail, c_tail = items

    n_tails.append([])
    for i in range(len(aln)):
        if i == row:
            n_tails[-1].append(n_tail)
        else:
            n_tails[-1].append('-'*len(n_tail))

    c_tails.append([])
    for i in range(len(aln)):
        if i == row:
            c_tails[-1].append(c_tail)
        else:
            c_tails[-1].append('-'*len(c_tail))

    row = row + 1

# print(n_tails)
# print(c_tails)
for name, items in zip(out_names,  zip(*n_tails, [seq for name, seq in aln], *c_tails) ):
    seq = ''.join(items)
    print('>'+name+'\n'+seq)

