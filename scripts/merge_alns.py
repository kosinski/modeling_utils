import sys
import itertools
from optparse import OptionParser

import sys
from itertools import groupby

def fasta_iter(fh):
    """
    given a fasta handle. yield tuples of header, sequence
    """
    # ditch the boolean (x[0]) and just keep the header or sequence since
    # we know they alternate.
    faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
    for header in faiter:
        # drop the ">"
        header = next(header)[1:].strip()
        # join all sequence lines to one.
        seq = "".join(s.strip() for s in next(faiter))
        seq = seq
        yield header, seq

def main():
    usage = 'usage: python %prog [options] out_seq_name1,out_seq_name2 <aln1.fasta> <aln2.fasta> ... > SpY.fasta"'
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()
    # parser.add_option("-o", "--out", dest="out_fn", default=None,
    #                   help="output filename")
    
    seqnames = args[0].split(',')
    fnames = args[1:]

    seqs = []
    for fn in fnames:
        with open(fn, "rU") as handle:
            seqs.append([s[1] for s in fasta_iter(handle)])

    outseqs = []
    for frags in zip(*seqs):
        outseqs.append('/'.join(frags))

    for i, seq in enumerate(outseqs):
        print('>{0}'.format(seqnames[i].replace('\n','')))
        print(seq)

if __name__ == '__main__':
    main()