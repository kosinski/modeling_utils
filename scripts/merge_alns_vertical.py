import sys
import itertools
from optparse import OptionParser

from Bio import SeqIO

def main():
    usage = 'usage: python %prog [options] <aln1.fasta> <aln2.fasta> ..."'
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()
    # parser.add_option("-o", "--out", dest="out_fn", default=None,
    #                   help="output filename")
    
    
    fnames = args[0:]

    seqs = []
    names = []
    for fn in fnames:
        handle = open(fn, "rU")
        newnames = []
        newseqs = []
        for i, record in enumerate(SeqIO.parse(handle, "fasta")):
            newseqs.append(str(record.seq))
            if i == 0 and names:
                names[i] = names[i] + '_' + record.description
            else:
                newnames.append(record.description)

        handle.close()

        if not seqs:
            seqs.extend(newseqs)
        else:
            oldlen = len(seqs[0])
            newlen = len(newseqs[0])

            #merge target
            seqs[0] = seqs[0] + '/' + newseqs[0]

            #add templates
            for i in range(1, len(seqs)):
                seqs[i] = seqs[i] + '/' + '-'*newlen

            for i in range(0, len(newseqs)):
                newseqs[i] = '-'*oldlen + '/' + newseqs[i]

            seqs.extend(newseqs[1:])

        names.extend(newnames)

    for name, seq in zip(names, seqs):
        print('>' + name)
        print(seq)

if __name__ == '__main__':
    main()