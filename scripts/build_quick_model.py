import sys
import os
import shutil
import tempfile
from optparse import OptionParser

from modeling_utils import modeller_utils

def guess_template():
    pass

def main():
    usage = "usage: %prog [options] aln_fasta_filename out_pdb_model_name <atom_files_dirs>"
    parser = OptionParser(usage=usage)

    parser.add_option("--skip_tails", action="store_true", dest="skip_tails", default=False,
                      help="skip_tails [default: %default]")

    parser.add_option("--skip_inserts", action="store_true", dest="skip_inserts", default=False,
                      help="skip_inserts [default: %default]")

    parser.add_option("--skipNter", action="store_true", dest="skipNter", default=False,
                      help="skipNter [default: %default]")

    parser.add_option("--skipCter", action="store_true", dest="skipCter", default=False,
                      help="skipCter [default: %default]")

    parser.add_option("--include", dest="include_regions",
                      help="File with sequence and template regions to include in modeling")

    parser.add_option("--optimize", action="store_true", dest="optimize", default=False,
                      help="optimize, do not use very_fast option [default: %default]")

    parser.add_option("--max_ca_ca_distance", type="int", dest="max_ca_ca_distance", default=14,
                      help="max_ca_ca_distance [default: %default]")

    parser.add_option("--flanks", type="int", dest="flanks", default=0,
                      help="flanks [default: %default]")

    parser.add_option("--models", type="int", dest="ending_model", default=1,
                      help="number of output models [default: %default]")

    (options, args) = parser.parse_args()

    aln_filename = args[0]
    out_filename = args[1]

    if len(args) > 2:
        atom_files_dirs = [os.path.abspath(path) for path in args[2:]]
    else:
        atom_files_dirs = [os.path.abspath('./')]

    with open(aln_filename) as f:
        aln_data = modeller_utils.aln_file_to_aln_data(f)


    seq_name = aln_data[0][0]

    knowns = []
    for t in aln_data[1:]:
        knowns.append(t[0])
    knowns = tuple(knowns)


    piraln, num_map, aln_obj = modeller_utils.create_modeller_pir(aln_data,
        chain_config=None,
        skip_tails=options.skip_tails,
        skip_inserts=options.skip_inserts,
        skipNter=options.skipNter,
        skipCter=options.skipCter,
        flanks=options.flanks,
        ligands=False,
        ligands_cfg=None,
        pdbfilenames=None
        # include_regions=options.include_regions
        )

    print(piraln)
    pirfilehandle, pirfilename = tempfile.mkstemp()
    os.close(pirfilehandle)
    with open(pirfilename, 'w') as pirfile:
        pirfile.write(piraln)
    
    tempdir = tempfile.mkdtemp()

    try:
        very_fast = not options.optimize

        out_model_filenames = modeller_utils.run_modeller(pirfilename, knowns,
                                                            very_fast=very_fast,
                                                            sequence=seq_name,
                                                            cwd=tempdir,
                                                            atom_files_dirs=atom_files_dirs,
                                                            with_breaks=True,
                                                            ending_models=options.ending_model,
                                                            ligands=None,
                                                            num_map=num_map,
                                                            max_ca_ca_distance=options.max_ca_ca_distance)

        if len(out_model_filenames) == 1:
            shutil.copy(out_model_filenames[0], out_filename)
        else:
            for i, out_model in enumerate(out_model_filenames):
                shutil.copy(out_model, out_filename.replace('.pdb', '{0}.pdb'.format(i)))

    finally:
        os.remove(pirfilename)
        shutil.rmtree(tempdir)

if __name__ == '__main__':
    main()