import sys

from modeling_utils import modeller_utils

def main():
    if len(sys.argv) < 2:
        print('Usage: python {0} pdbfilename'.format(sys.argv[0]))
        print('or')
        print('python {0} pdbfilename --fasta'.format(sys.argv[0]))
        sys.exit()
    pdbfilename = sys.argv[1]
    out_seq = modeller_utils.extract_modeller_atom_seq(pdbfilename)
    if len(sys.argv) == 3 and sys.argv[2] == '--fasta':
        print('>' + pdbfilename)
        print(out_seq)
    else:
        print(out_seq)

if __name__ == '__main__':
    main()